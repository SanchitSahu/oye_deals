var base_url = window.location.protocol + "//" + window.location.host + "/" + 'oye_deals/';
$(document).on('click', '#business_type_submit', function () {
    var business_type_name = $('#nature_bussiness').val();
    //var status_bussines = $('input[name=status_bussiness]:radio:checked').val();
    var status_bussines = $('input[name=status_bussiness]:checked').val();
    var mode = $('#business_type_submit').attr('mode');
    var upid = $('#business_type_submit').attr('upid');
    if (business_type_name != '' && status_bussines != '') {
        $.ajax({
            type: 'POST',
            url: base_url + "admin/add_business_type",
            data: {business_type_name: business_type_name,
                status_bussines: status_bussines,
                mode: mode,
                upid: upid
            },
            success: function (data) {
                if (data == 1) {
                    swal({
                        title: "Success",
                        text: "Nature of bussiness added successfully.",
                        type: "success"
                    });
                    $('#add_bus_type').fadeOut('slow');
                    $('.modal-backdrop').fadeOut('slow');
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                } else if (data == 'true')
                {
                    swal({
                        title: "Success",
                        text: "Nature of bussiness updated successfully.",
                        type: "success"
                    });
                    $('#add_bus_type').fadeOut('slow');
                    $('.modal-backdrop').fadeOut('slow');
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                } else {
                    swal({
                        title: "Alert",
                        text: "Ops! Something went worng please try again.",
                        type: "success"
                    });
                }
            }
        });
        return false;
    }
});
function validaeForm(frmName, rulesObj, messageObj, groupObj)
{
    // validate form
    $('#' + frmName).validate({
        ignore: '',
        onkeyup: false,
        errorClass: "text-error",
        validClass: "text-success",
        rules: rulesObj,
        messages: messageObj,
        groups: groupObj,
        invalidHandler: function (form, validator) {
        },
        showErrors: function (errorMap, errorList) {

            // create array of error list string

            var strElement = $.param(errorMap).split("&");



            // check error string is blank or not
            if ($.trim(strElement) != "")
            {
                // get id of first element
                ///following one line is added by keyur
                strElement[0] = unescape(strElement[0]);

                var arrName = strElement[0].split("=");

                // get element id

                var eleId = $('[name="' + arrName[0] + '"]').attr("id");

                // find tab div id of form element
                if (eleId == undefined)
                {///this condition is added by keyur
                    var parentId = $('[name="' + arrName[0] + '"]').parents('div[id]').parents('div[id]').attr("id");
                } else
                    var parentId = $("#" + eleId).parents('div[id]').attr("id");


                // active tab 
                $('a[href|="#' + parentId + '"]').tab("show");

            }

            // show default error message
            this.defaultShowErrors();
        }
    });
}
bus_type_table = $('#business_types').DataTable({
    dom: 'l<"html5buttons"B>tr<"col-md-5"i><"col-sm-7"p>',
    buttons: [
        {extend: 'excel', title: 'BusinessTypes'},
        {extend: 'pdf', title: 'BusinessTypes',
            customize: function (doc) {
                doc.styles.table = {
                    width: '100%',
                }
                doc.styles.tableHeader.alignment = 'left';
                doc.content[1].table.widths = '*';
            }
        },
        {extend: 'print',
            customize: function (win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
            }
        }
    ]

});

function changeStatus(url, id, status) {
    if (status == 1) {
        newStatus = 0;
    } else {
        newStatus = 1;
    }

    $.ajax({
        "type": "post",
        "url": url,
        "data": ("status[" + id + "]=" + newStatus),
        "success": function (data) {
            var objData = jQuery.parseJSON(data);
            if (objData.message == "Success") {
                alert("Status changed  successfully");
                window.location.reload();
            } else {
                alert("Error in updating status");
            }
        }
    });
}

$(document).ready(function () {
    jQuery.fn.datatableCustomization = function () {
        var excelTitle = $(this).attr('data-excelTitle');
        var pdfTitle = $(this).attr('data-pdfTitle');
        var filterKey = $(this).attr('data-filterKey')
        var dtTable = $(this).DataTable({
            dom: 'l<"html5buttons"B>tr<"col-md-5"i><"col-sm-7"p>',
            buttons: [
                {extend: 'excel', title: excelTitle},
                {extend: 'pdf', title: pdfTitle,
                    customize: function (doc) {
                        doc.styles.table = {
                            width: '100%',
                        }
                        doc.styles.tableHeader.alignment = 'left';
                        doc.content[1].table.widths = '*';
                    }
                },
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]

        });
        if ($("#" + filterKey)) {
            $("#" + filterKey).on('keyup', function () {
                dtTable.search(this.value).draw();
            });
        }
    }

    if ($("#app_users_list")) {
        $("#app_users_list").datatableCustomization();
    }
    if ($("#business_users_list")) {
        $("#business_users_list").datatableCustomization();
    }
});
$(document).on('click', '#business_types tbody tr td:last-child button', function () {
    var id = $(this).attr('id');
    if (id != '') {
        $.ajax({
            type: 'POST',
            url: base_url + "admin/get_business_type",
            data: {id: id},
            success: function (data) {
                var resp = $.parseJSON(data);
                if (resp != 0) {
                    $('#add_bus_type').modal('show');
                    $('#nature_bussiness').val(resp[0].business_type_name);
                    $('#status_bussiness').val(resp[0].status);
                    $('#business_type_submit').attr('mode', 'update');
                    $('#business_type_submit').attr('upid', id);
                } else {
                    swal({
                        title: "Alert",
                        text: "Ops! Something went worng please try again.",
                        type: "success"
                    });
                }
            }
        });
    }

});

$(document).on('click', '#analytics_submit', function () {

    var coupon_type = [];
   $("input[name='coupontype[]']:checked").each(function () {
        var id = $(this).val();
        coupon_type.push(id);
    });
    // alert(coupon_type);return false;
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();

    if ($('#analyticsForm').valid()) {
        $.ajax({
            type: 'POST',
            url: base_url + "admin/grab_analytics",
            data: {coupon_type: coupon_type,
                start_date: start_date,
                end_date: end_date},
            success: function (data) {
                $('.table-responsive').html('');
                $('.table-responsive').append(data);
            }
        });
    } else {
        return false;
    }
    return false;
});

$('#data_5 .input-daterange').datepicker({
    keyboardNavigation: false,
    forceParse: false,
    autoclose: true
});