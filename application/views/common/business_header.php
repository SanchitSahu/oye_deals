<!DOCTYPE html>
<html lang="en" class="no-scroll">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0;">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>Oye Deals </title>
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets'); ?>/img/favicon.png">
        <link href="<?php echo base_url('assets'); ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
        <!-- Toastr style -->
        <link href="<?php echo base_url('assets'); ?>/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <!-- Plugins -->
        <!--<link href="<?php echo base_url('assets'); ?>/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">-->

        <link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/icheck.css" rel="stylesheet">


        <script src="<?php echo base_url('assets'); ?>/js/jquery-2.1.1.js"></script>
        <link href="<?php echo base_url('assets'); ?>/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/cropper/cropper.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/switchery/switchery.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

        <!--link href="<?php echo base_url('assets'); ?>/css/plugins/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet"-->

        <link href="<?php echo base_url('assets'); ?>/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/select2/select2.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/summernote/summernote.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/animate.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/dev_styles.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/chosen/chosen.css" rel="stylesheet">
        <!--link href="<?php echo base_url('assets'); ?>/css/plugins/jquery-highlighttextarea-master/jquery.highlighttextarea.min.css" rel="stylesheet"-->
        <link href="<?php echo base_url('assets'); ?>/css/plugins/dropzone/basic.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/plugins/dropzone/dropzone.css" rel="stylesheet">
        <!--link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/lightbox.min.css"-->
    </head>
    <body>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element"> <center><span>
                                    <?php
                                        if(!empty($this->session->userdata('BusinessUserLogo')))
                                        {
                                    ?>
                                         <img alt="image" class="img-circle" src="<?php echo base_url(); ?>assets/profileimages/<?php echo $this->session->userdata('BusinessUserLogo'); ?>" height="70px;" width="70px;"/>
                                    <?php
                                        }
                                        else {
                                    ?>
                                        <img alt="image" class="img-circle" src="<?php echo base_url(); ?>assets/images/nouser.jpg" height="70px;" width="70px;"/>
                                    <?php
                                        }
                                    ?>
                                </span> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $this->session->userdata('BusinessUserName'); ?></strong> </span></center>
                            </div>
                        </li>
                        <li <?php if($this->uri->uri_string == 'business'){ echo 'class="active"'; } ?> ><a href="<?php echo base_url(); ?>/business"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span> </a> </li>
                        <li <?php if($this->uri->uri_string == 'business/offers'){ echo 'class="active"'; } ?>> <a href="<?php echo base_url(); ?>business/offers"><i class="fa fa-tags"></i> <span class="nav-label">Manage Offers</span></a> </li>
                        <li <?php if($this->uri->uri_string == 'business/customers'){ echo 'class="active"'; } ?>> <a href="<?php echo base_url().'business/customers' ?>"><i class="fa fa-users"></i> <span class="nav-label">Manage Customers</span></a> </li>
                        <li <?php if($this->uri->uri_string == 'business/analytics'){ echo 'class="active"'; } ?>> <a href="#"><i class="fa fa-pie-chart"></i> <span class="nav-label">Analytics</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="">Page1</a></li>
                                <li><a href="">Page2</a></li>
                                <li><a href="">Page3</a></li>
                            </ul>
                        </li>
                        <li <?php if($this->uri->uri_string == 'business/viewprofile'){ echo 'class="active"'; } ?>> <a href="<?php echo base_url() . 'business/viewprofile' ?>"><i class="fa fa-desktop"></i> <span class="nav-label">Profile</span></a> </li>
                        <li <?php if($this->uri->uri_string == 'business/logout'){ echo 'class="active"'; } ?>> <a href="<?php echo base_url() . 'business/logout' ?>"><i class="fa fa-user"></i> <span class="nav-label">Logout</span></a> </li>
                    </ul>

                </div>
            </nav>