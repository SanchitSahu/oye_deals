<!DOCTYPE html>
<html lang="en" class="no-scroll">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0;">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>Oye Deals </title>
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets'); ?>/img/favicon.png">
        <link href="<?php echo base_url('assets'); ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
        <!-- Toastr style -->
        <link href="<?php echo base_url('assets'); ?>/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <!-- Plugins -->
        <!--<link href="<?php echo base_url('assets'); ?>/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">-->

        <link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/icheck.css" rel="stylesheet">

        <script src="<?php echo base_url('assets'); ?>/js/jquery-2.1.1.js"></script>

        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">-->

        <link href="<?php echo base_url('assets'); ?>/css/plugins/cropper/cropper.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/switchery/switchery.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<!--        <link href="<?php echo base_url('assets'); ?>/css/plugins/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet">-->

        <link href="<?php echo base_url('assets'); ?>/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/select2/select2.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/summernote/summernote.css" rel="stylesheet">
        <!--<link href="<?php echo base_url('assets'); ?>/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">-->

        <link href="<?php echo base_url('assets'); ?>/css/animate.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/dev_styles.css" rel="stylesheet">
<!--        <link href="<?php echo base_url('assets'); ?>/css/dev_styles.css" rel="stylesheet">-->

        <link href="<?php echo base_url('assets'); ?>/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/chosen/chosen.css" rel="stylesheet">
<!--        <link href="<?php echo base_url('assets'); ?>/css/plugins/jquery-highlighttextarea-master/jquery.highlighttextarea.min.css" rel="stylesheet">-->
        <link href="<?php echo base_url('assets'); ?>/css/plugins/dropzone/basic.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/plugins/dropzone/dropzone.css" rel="stylesheet">
<!--        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/lightbox.min.css">-->
    </head>
    <body>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element"> <span>
                                    <img alt="image" class="img-circle" src="<?php echo base_url('assets'); ?>/img/profile_small.jpg" />
                                </span>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo ucfirst($this->session->userdata('admin_user_data')['name']); ?></strong>
                                        </span> </a>
                            </div>
                            <div class="logo-element">
                                IN+
                            </div>
                        </li>
                        <li <?php if($this->uri->uri_string == 'admin/dashboard'){ echo 'class="active"'; } ?>>
                            <a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span> </a>

                        </li>
                        <!-- <li>
                            <a href="<?php echo base_url(); ?>admin/customer"><i class="fa fa-diamond"></i> <span class="nav-label">Customer List</span></a>
                        </li> -->
                        
                        <li >
                            <a href="javascript:void(0);"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Manage Users</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li <?php if($this->uri->uri_string == 'admin/app_users'){ echo 'class="active"'; } ?>>
                                    <a href="<?php echo base_url(); ?>admin/app_users"><i class="fa fa-globe"></i> <span class="nav-label">App Users</span></a>
                                </li>

                            </ul>
                        </li>
                        
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Manage Businesses</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li <?php if($this->uri->uri_string == 'admin/business_users'){ echo 'class="active"'; } ?>>
                                <a href="<?php echo base_url(); ?>admin/business_users"><i class="fa fa-pie-chart"></i> <span class="nav-label">Business Users</span></a>
                                </li>
                                <li <?php if($this->uri->uri_string == 'admin/business_type'){ echo 'class="active"'; } ?>>
                                    <a href="<?php echo base_url(); ?>admin/business_type"><i class="fa fa-globe"></i> <span class="nav-label">Business Type</span></a>
                                </li>
                            </ul>
                        </li>
                        <li <?php if($this->uri->uri_string == 'admin/offers'){ echo 'class="active"'; } ?>>
                            <a href="<?php echo base_url();?>admin/offers"><i class="fa fa-envelope"></i> <span class="nav-label">Offers </span></a>
                        </li>

                        <li <?php if($this->uri->uri_string == 'admin/analytics'){ echo 'class="active"'; } ?>>
                            <a href="<?php echo base_url(); ?>admin/analytics"><i class="fa fa-edit"></i> <span class="nav-label">Analytics</span></a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                            <form role="search" class="navbar-form-custom" action="search_results.html">
<!--                                <div class="form-group">
                                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                                </div>-->
                            </form>
                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <span class="m-r-sm text-muted welcome-message">Welcome to Oye Deals Adminstration.</span>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                    <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                                </a>
                                <ul class="dropdown-menu dropdown-messages">
                                    <li>
                                        <div class="dropdown-messages-box">
                                            <a href="profile.html" class="pull-left">
                                                <img alt="image" class="img-circle" src="<?php echo base_url('assets/img/a7.jpg'); ?>">
                                            </a>
                                            <div>
                                                <small class="pull-right">46h ago</small>
                                                <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                                <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <div class="dropdown-messages-box">
                                            <a href="profile.html" class="pull-left">
                                                <img alt="image" class="img-circle" src="<?php echo base_url('assets/img/a7.jpg'); ?>">
                                            </a>
                                            <div>
                                                <small class="pull-right text-navy">5h ago</small>
                                                <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                                <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <div class="dropdown-messages-box">
                                            <a href="profile.html" class="pull-left">
                                                <img alt="image" class="img-circle" src="<?php echo base_url('assets/img/profile.jpg'); ?>">
                                            </a>
                                            <div>
                                                <small class="pull-right">23h ago</small>
                                                <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                                <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <div class="text-center link-block">
                                            <a href="mailbox.html">
                                                <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                    <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                                </a>
                                <ul class="dropdown-menu dropdown-alerts">
                                    <li>
                                        <a href="mailbox.html">
                                            <div>
                                                <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                                <span class="pull-right text-muted small">4 minutes ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="profile.html">
                                            <div>
                                                <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                                <span class="pull-right text-muted small">12 minutes ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="grid_options.html">
                                            <div>
                                                <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                                <span class="pull-right text-muted small">4 minutes ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <div class="text-center link-block">
                                            <a href="notifications.html">
                                                <strong>See All Alerts</strong>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="<?php echo base_url('admin') . '/logout'; ?>">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                            <li>
                                <a class="right-sidebar-toggle">
                                    <i class="fa fa-tasks"></i>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>