<div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header"> <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                <form role="search" class="navbar-form-custom" action="search_results.html">
<!--                    <div class="form-group">
                        <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                    </div>-->
                </form>
            </div>
        </nav>
    </div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Offer Details</h5>

                </div>
                <div class="ibox-content">
                    <div class="row">
                        <form>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <select class="form-control" id="sel1">
                                        <option value="">-- Select Offer Type --</option>
                                        <?php foreach ($offer_type as $key => $value) { ?>
                                            <option value="<?php echo $value['offer_type_id']; ?>" <?php echo ($value['offer_type_id'] == $offer_data[0]['offer_type_id']) ? "selected" : ""; ?>> <?php echo $value['offer_type_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="offer_discount"  id="offer_discount" placeholder="Enter discount figure" value="<?php echo $offer_data[0]['offer_discount']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="title" placeholder="Title" value="<?php echo $offer_data[0]['offer_title']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group" id="data_5">
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input type="text" class="input-sm form-control" name="start" value="<?php echo date('d-m-Y', strtotime($offer_data[0]['offer_startdate'])); ?>"/>
                                        <span class="input-group-addon">to</span>
                                        <input type="text" class="input-sm form-control" name="end" value="<?php echo date('d-m-Y', strtotime($offer_data[0]['offer_enddate'])); ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="form-group">
                                    <textarea class="form-control" rows="5" id="desc" placeholder="Description"><?php echo $offer_data[0]['offer_desc']; ?></textarea>
                                </div>
                            </div>
                            <div class="col-sm-10 col-sm-offset-1">
                                <p style="text-align:left;">Location:</p>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="area" placeholder="Area" value="<?php echo $offer_data[0]['offer_area']; ?>" />
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="radius" name="radius" placeholder="Radius" value="<?php echo $offer_data[0]['radius']; ?>">
                                </div>
                                <div class="form-group">
                                    <!--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14687.720961148056!2d72.51596505!3d23.02633345!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1501751896957" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <br/>
                                <br/>
                                <div class="form-group">
                                    <label><u>Available Coupons</u></label>
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Coupon Code </th>
                                                    <th>QR Code Link</th>
                                                    <th>Assigned to </th>
                                                    <th>Status </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($offer_coupon_data as $key => $data) {
                                                    $html = "<tr>";
                                                    $html .= "<td>" . ++$key . "</td>";
                                                    $html .= "<td>{$data['coupon_code']}</td>";
                                                    $html .= "<td><a target='_blank' href='" . base_url('assets/qr_codes/') . $data['offer_coupon_id'] . ".png" . "'>QR Code Link</a></td>";
                                                    $html .= "<td>" . ($data['user_id'] != '' ? $data['username'] . "(UserId: " . $data['user_id'] . ")" : "UnAssigned") . "</td>";
                                                    $html .= "<td>{$data['status_text']}</td>";
                                                    $html .= "</tr>";
                                                    echo $html;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><script type="text/javascript">
    $(document).ready(function () {
        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "dd-mm-yyyy"
        });

        $('#data_2 .input-group.date').datepicker({
            startView: 1,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd-mm-yyyy"
        });

        $('#data_3 .input-group.date').datepicker({
            startView: 2,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd-mm-yyyy"
        });

        $('#data_4 .input-group.date').datepicker({
            minViewMode: 1,
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            todayHighlight: true,
            format: "dd-mm-yyyy"
        });

        $('#data_5 .input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd-mm-yyyy"
        });
        
        
    });
</script>