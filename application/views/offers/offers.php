<?php //echo '<pre>'; print_r($this->session->userdata('BusinessUserId')); exit;      ?>
<div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header"> <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                <form role="search" class="navbar-form-custom" action="search_results.html">
                    <!--                    <div class="form-group">
                                            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                                        </div>-->
                </form>
            </div>
        </nav>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Available Offers</h5>
                        <div class="ibox-tools">
                            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addoffers">Add Offers</button>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title </th>
                                        <th>Description </th>
                                        <th>Available Coupons</th>
                                        <th>Captured Coupons</th>
                                        <th>Status</th>
                                        <th>More</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($all_offer as $key => $data) {
                                        $html = "<tr>";
                                        $html .= "<td>" . ++$key . "</td>";
                                        $html .= "<td>{$data['business_name']}</td>";
                                        $html .= "<td>{$data['offer_title']}</td>";
                                        $html .= "<td>{$data['coupon_count']['availableCount']}</td>";
                                        $html .= "<td>{$data['coupon_count']['capturedCount']}</td>";
                                        $html .= "<td>{$data['status_text']}</td>";
                                        $html .= "<td> <a class='btn btn-sm btn-primary' href='" . base_url('business/manage_offers/' . $data['offer_id']) . "'>Details</a></td>";
                                        $html .= "</tr>";
                                        echo $html;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add Coupons Modal -->
    <div class="modal fade" id="addoffers" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Offer</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <form id="addOffer" method="POST" action="<?php echo base_url('business/save_offer_details'); ?>" enctype="multipart/form-data">
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <select class="form-control" id="sel1" name="offer_type_id">
                                        <option value="">-- Select Offer Type --</option>
                                        <?php foreach ($offer_type as $key => $value) { ?>
                                            <option value="<?php echo $value['offer_type_id']; ?>" ><?php echo $value['offer_type_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <input type="number" class="form-control" name="offer_discount" min="0" id="offer_discount" placeholder="Enter discount figure">
                                </div>
                            </div>

                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="offer_title" name="offer_title" placeholder="Title">
                                </div>
                                <br/>
                                <p style="text-align:left;">Offer Image:</p>
                                <div class="form-group">
                                    <input type="file" accept="image/*" name="offer_image" />
                                </div><br>
                                <div class="form-group">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="5" name="offer_desc"  id="offer_desc" placeholder="Description"></textarea>
                                    </div>
                                </div>
                                <div class="form-group" id="data_5" align="center">
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input type="text" class="input-sm form-control" name="offer_startdate" id="offer_startdate" placeholder="Start Date"/>
                                        <span class="input-group-addon">to</span>
                                        <input type="text" class="input-sm form-control" name="offer_enddate" id="offer_enddate" placeholder="End Date" />
                                    </div>
                                </div>
                                <br/>
                                <p style="text-align:left;">Bulk Upload Coupons Using <a target="_blank" href="<?php echo base_url('assets/couponSampleData.csv'); ?>">This CSV</a>:</p>
                                <div class="form-group">
<!--                                    <input type="file" accept=".csv, text/csv" name="couponData" />-->
                                    <input type="file" accept=".csv,application/vnd.ms-excel,text/csv" name="couponData" />
                                </div><br>
                                <p style="text-align:left;">Location:</p>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="offer_area" name="offer_area" placeholder="Area">
                                    <input type="hidden" data-geo="location" name="location" readonly >
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" id="radius" min="0" name="radius" placeholder="Radius(in Kms)">
                                </div>
                                <!--<div class="form-group">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14687.720961148056!2d72.51596505!3d23.02633345!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1501751896957" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>-->
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <button type="reset" class="btn btn-danger close_offer_popup">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAKf3mILelPJRBL8ksWrskqVRJ623Wy9Fo"></script>-->
    <script src="<?php echo base_url('assets'); ?>/js/jquery.geocomplete.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
<?php if ($this->session->flashdata('success')) { ?>
                Command: toastr["success"]("<?php echo $this->session->flashdata('success'); ?>")
<?php } ?>
<?php if ($this->session->flashdata('error')) { ?>
                Command: toastr["error"]("<?php echo $this->session->flashdata('error'); ?>")
<?php } ?>


            $('#addOffer').validate({
                ignore: [],
                rules: {
                    offer_type_id: {
                        required: true
                    },
                    offer_discount: {
                        required: true,
                        max: {
                            depends: function(element){
                                return ($("#sel1").val() == '1' && $("#offer_discount").val() > 100);
                                /*console.log($("#sel1").val());
                                if($("#sel1").val() == 1) {

                                }*/
                            }
                        }
                    },
                    offer_title: {
                        required: true
                    },
                    offer_desc: {
                        required: true
                    },
                    offer_startdate: {
                        required: true
                    },
                    offer_enddate: {
                        required: true
                    },
                    location: {
                        required: true
                    },
                    couponData: {
                        required: true
                    },
                    radius: {
                        required: true
                    }
                },
                messages: {
                    offer_type_id: {
                        required: "Please select Offer type"
                    },
                    offer_discount: {
                        required: "Please enter discount Value",
                        max: "Max amount would be 100 only"
                    },
                    offer_title: {
                        required: "Please enter Offer Title"
                    },
                    offer_desc: {
                        required: "Please enter Offer Description"
                    },
                    offer_startdate: {
                        required: "Please select Start Date"
                    },
                    offer_enddate: {
                        required: "Please select End Date"
                    },
                    location: {
                        required: "Please select location from the autocomplete dropdown"
                    },
                    couponData: {
                        required: "Please upload a csv file to bulk upload coupons",
                        accept: "Please upload valid CSV file"
                    },
                    radius: {
                        required: "Please enter radius"
                    }
                }
            });


            $("#offer_area").geocomplete({
                details: "form .form-group",
                detailsAttribute: "data-geo"
            });

            $('.close_offer_popup').click(function () {
                $('#addoffers').modal('hide');
            });


            $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: "dd-mm-yyyy"
            });

            $('#data_2 .input-group.date').datepicker({
                startView: 1,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                format: "dd-mm-yyyy"
            });

            $('#data_3 .input-group.date').datepicker({
                startView: 2,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                format: "dd-mm-yyyy"
            });

            $('#data_4 .input-group.date').datepicker({
                minViewMode: 1,
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                todayHighlight: true,
                format: "dd-mm-yyyy"
            });

            $('#data_5 .input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                format: "dd-mm-yyyy"
            });
        });
    </script>