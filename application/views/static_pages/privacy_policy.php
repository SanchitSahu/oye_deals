<h1><strong>Privacy Policy</strong></h1>

<p><strong>&nbsp;</strong></p>

<p>This privacy policy discloses our privacy practices. This privacy policy applies solely to information collected by Oye.Deals website and our Android and iOS apps.</p>

<p>&nbsp;</p>

<p><strong>Information Collection, Use, and Sharing</strong></p>

<p>We are the sole owners of the information collected on this site. We will not sell or rent this information to anyone. We only gather information regarding demographics, including location, gender, age, and interests. This information is used to improve the content of the website, mobile apps and the products we offer. We will not share your information with any third party. Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.</p>

<p>&nbsp;</p>

<p><strong>Choice</strong></p>

<p>You can choose what data you make available through Ads Settings, Ad Settings for mobile apps, or any other available means (for example, the NAI&rsquo;s consumer opt-out).</p>

<p>&nbsp;</p>

<p><strong>Access</strong></p>

<p>As an individual, you may exercise your right to access the data held about you by this company by submitting your request in writing. Reasonable efforts will be made to keep your information updated. We request that you contact us if you&rsquo;d like to verify or change your information. You also have the right to request the blocking or erasure of data which has been processed unlawfully.</p>

<p>&nbsp;</p>

<p><strong>Security</strong></p>

<p>We take precautions to protect your information. When you visit the website, use mobile apps or submit information, your information is protected both online and offline.</p>

<p>Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure AWS environment.</p>

<p>&nbsp;</p>

<p><strong>Cookies</strong></p>

<p>We use &ldquo;cookies&rdquo; on this site. A cookie is a piece of data stored on a site visitor&rsquo;s hard drive to help us improve your access to our site and identify repeat visitors to our site. Cookies can also enable us to track and target the interests of our users to enhance the experience on our site. Usage of a cookie is in no way linked to any personally identifiable information on our site.</p>

<p>&nbsp;</p>

<p><strong>Browsers and Devices</strong></p>

<p>Certain data are collected automatically from the web browser and web server you use: IP addresses, browser types (Firefox, Chrome etc.), browser versions, the content you are visiting, and the type of device being used (iPhone, Samsung tablet, etc.)</p>

<p>&nbsp;</p>

<p><strong>Links</strong></p>

<p>This website may contain links to other sites. We are not responsible for the content or privacy practices of such other sites. We encourage our users to be aware when they leave our site and to read the privacy statements of any other site that collects personally identifiable information.</p>

<p>&nbsp;</p>

<p><strong>Surveys &amp; Contests</strong></p>

<p>We may request information from you via surveys or contests. Participation in these surveys or contests is voluntary and you may choose whether or not to participate and therefore disclose this information. Survey information will be used for purposes of monitoring or improving the use and satisfaction of this site and mobile apps.</p>

<p>&nbsp;</p>

<p><strong>Updates</strong></p>

<p>Our Privacy Policy may change from time to time and all updates will be posted on this page.</p>

<p>&nbsp;</p>

<p>If you feel that we are not abiding by this privacy policy, you should contact us immediately at <a href="info@oye.deals">info@oye.deals</a>.</p>