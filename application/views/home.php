<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>

<html lang="en">

    <head>

        <!-- Html Page Specific -->
        <meta charset="utf-8">
        <title>Oye Deals!</title>

        <!-- Mobile Specific -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

        <!--[if lt IE 9]>
    <script type="text/javascript" src="scripts/html5shiv.js"></script>
<![endif]-->

        <!-- CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css-lp/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css-lp/animate.css" />
        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css-lp/simple-line-icons.css" />
        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css-lp/icomoon-soc-icons.css" />
        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css-lp/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css-lp/magnific-popup.css" />
        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css-lp/style.css" />
        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css-lp/custom-lp.css" />
    </head>

    <body data-spy="scroll" data-target=".navMenuCollapse">

        <!-- PRELOADER -->
        <div id="preloader">
            <div class="battery inner">
                <div class="load-line"></div>
            </div>
        </div>

        <div id="wrap">

            <div class="flex-container">
                <div class="flex-item">
                    <div class="screeens">
                        <img src="<?php echo base_url('assets'); ?>/images/screens.png" />
                    </div>
                </div>
                <div class="flex-item">
                    <div class="stuffs">
                        <img src="<?php echo base_url('assets'); ?>/images/logo.png" class="logo" />
                        <div class="title">
                            <h1>Augmented Reality App</h1>
                            <h3>Get exclusive discounts on premier local brands</h3>
                        </div>
                        <div class="groups">
                            <img src="<?php echo base_url('assets'); ?>/images/groups.png" />
                        </div>
                        <div class="stores">
                            <a href="#"><img src="<?php echo base_url('assets'); ?>/images/appstore.png" /></a>
                            <a href="#"><img src="<?php echo base_url('assets'); ?>/images/playstore.png" /></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer">
                <div class="row">
                    <div class="col-sm-12">
                        Copyright &copy; <?php date('Y'); ?>. Powered by Solulab Inc.
                        <a href="<?php echo base_url('privacy'); ?>">Privacy policy</a> 
                    </div>
                </div>
            </div>

        </div>


        <!-- JavaScript -->
        <script src="<?php echo base_url('assets'); ?>/scripts/jquery-1.8.2.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/scripts/bootstrap.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/scripts/owl.carousel.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/scripts/jquery.validate.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/scripts/wow.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/scripts/smoothscroll.js"></script>
        <script src="<?php echo base_url('assets'); ?>/scripts/jquery.smooth-scroll.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/scripts/jquery.superslides.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/scripts/placeholders.jquery.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/scripts/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/scripts/jquery.stellar.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/scripts/retina.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/scripts/typed.js"></script>
        <script src="<?php echo base_url('assets'); ?>/scripts/custom.js"></script>

        <!--[if lte IE 9]>
        <script src="scripts/respond.min.js"></script>
        <![endif]-->
    </body>

</html>
