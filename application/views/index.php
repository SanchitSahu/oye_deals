<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .error{
        color:red;
    }
</style>
<html lang="en">
    <head>
        <title>Oye Deals</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="<?php echo base_url('assets'); ?>/css/bootstrap.min.css" rel="stylesheet">
        <script src="<?php echo base_url('assets'); ?>/js/jquery-2.1.1.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/bootstrap.min.js"></script>

        <link href="<?php echo base_url('assets'); ?>/css/custom.css" rel="stylesheet">
    </head>
    <body>
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#signin">Log In</button>
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#signup">Sign Up</button>

        <!-- Log In Modal -->
        <div class="modal fade" id="signin" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Log In</h4>
                    </div>
                    <div class="modal-body"> <img src="<?php echo base_url('assets'); ?>/images/logo.png" alt="" class="logo" />
                        <form class="form-horizontal" method="post" id="loginForm" name="loginForm" action="<?php echo base_url() . 'business/login' ?>">
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <input type="email" class="form-control" id="email" placeholder="Email Address" name="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <input type="password" class="form-control" id="pwd" placeholder="Password" name="pwd">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-7 pull-right">
                                    <button type="submit" class="btn btn-primary">Log In</button>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#forgot" data-dismiss="modal">Forgot Password?</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Sign Up Modal -->
        <div class="modal fade" id="signup" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Sign up</h4>
                    </div>
                    <div class="modal-body"> <img src="<?php echo base_url('assets'); ?>/images/logo.png" alt="" class="logo" />
                        <form class="form-horizontal" method="post" name="signupForm" id="signupForm" action="<?php echo base_url() . 'business/register' ?>">
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <input type="text" class="form-control" id="bname" placeholder="Business Name" name="bname">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <input type="email" class="form-control" id="email" placeholder="Email Address" name="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <input type="password" class="form-control" id="pwd1" placeholder="Confirm Password" name="pwd1">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <input type="submit" name="Register" value="Register" class="btn btn-primary">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2 pull-right"> Already have an Account?&nbsp;
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#signin" data-dismiss="modal">Log In</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Forgot Password Modal -->
        <div class="modal fade" id="forgot" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Forgot Password</h4>
                    </div>
                    <div class="modal-body"> <img src="<?php echo base_url('assets'); ?>/images/logo.png" alt="" class="logo" />
                        <form class="form-horizontal" action="">
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <input type="email" class="form-control" id="email" placeholder="Email Address" name="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script src="<?php echo base_url('assets'); ?>/js/jquery-ui-1.10.4.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/jquery-validation/jquery.validate.min.js"></script>


<script>

    $(document).ready(function () {
        function validaeForm(frmName, rulesObj, messageObj, groupObj)
        {
            // validate form
            $('#' + frmName).validate({
                ignore: '',
                onkeyup: false,
                errorClass: "text-error",
                validClass: "text-success",
                rules: rulesObj,
                messages: messageObj,
                groups: groupObj,
                invalidHandler: function (form, validator) {
                },
                showErrors: function (errorMap, errorList) {

                    // create array of error list string

                    var strElement = $.param(errorMap).split("&");



                    // check error string is blank or not
                    if ($.trim(strElement) != "")
                    {
                        // get id of first element
                        ///following one line is added by keyur
                        strElement[0] = unescape(strElement[0]);

                        var arrName = strElement[0].split("=");

                        // get element id

                        var eleId = $('[name="' + arrName[0] + '"]').attr("id");

                        // find tab div id of form element
                        if (eleId == undefined)
                        {///this condition is added by keyur
                            var parentId = $('[name="' + arrName[0] + '"]').parents('div[id]').parents('div[id]').attr("id");
                        } else
                            var parentId = $("#" + eleId).parents('div[id]').attr("id");


                        // active tab 
                        $('a[href|="#' + parentId + '"]').tab("show");

                    }

                    // show default error message
                    this.defaultShowErrors();
                }
            });
        }
        var rules = {
            email: {required: true, email: true},
            pwd: {required: true, whitespaceValue: true, minlength: 5 /*passwordValue: true*/},
            pwd1: {required: true, whitespaceValue: true, minlength: 5 /*passwordValue: true*/},
        }
        var messages = {
            email: {
                required: "Please enter email address",
                email: "Please enter valid email address"
            },
            pwd: {
                required: "Please enter password",
                minlength: "Please enter minimum 5 character password",
                whitespaceValue: "White Space is not allowed for password",
                //passwordValue: "Password must have at least one uppercase letter, one digits and one special character"
            }
        }
        var rules1 = {
            bname: {required: true},
            email: {required: true, email: true},
            password: {required: true, whitespaceValue: true, minlength: 5 /*passwordValue: true*/},
            pwd1: {required: true, whitespaceValue: true, minlength: 5, equalTo: "#password"}

        }
        var messages1 = {
            bname: {required: "Please enter business name"},
            email: {
                required: "Please enter email address",
                email: "Please enter valid email address"
            },
            password: {
                required: "Please enter password",
                minlength: "Please enter minimum 5 character password",
                whitespaceValue: "White Space is not allowed for password",
                //passwordValue: "Password must have at least one uppercase letter, one digits and one special character"
            },
            pwd1: {
                required: "Please enter password",
                minlength: "Please enter minimum 5 character password",
                whitespaceValue: "White Space is not allowed for password",
                equalTo: "Please enter the same password as above"
            }
        }
        validaeForm('loginForm', rules, messages);
        validaeForm('signupForm', rules1, messages1);
    });
    $('#signin.modal').on('hidden.bs.modal', function () {
        $(this).find('input,textarea,select').each(function () {

            // $(this).trigger('change');
        });
    });
    $('#signup.modal').on('hidden.bs.modal', function () {
        $(this).find('input,textarea,select').each(function () {

            // $(this).trigger('change');
        });
    });
</script>
