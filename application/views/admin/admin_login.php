<!DOCTYPE html>
<html lang="en" class="no-scroll">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title><?php echo $pageTitle; ?></title>
        <script src="<?php echo base_url('assets'); ?>/js/jquery-2.1.1.js"></script>
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets'); ?>/img/favicon.png">

        <!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
        <link href="<?php echo base_url('assets'); ?>/css/bootstrap.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/icheck.css" rel="stylesheet">

        <link href="<?php echo base_url('assets'); ?>/css/plugins/chosen/chosen.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/plugins/select2/select2.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/plugins/toastr/toastr.min.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url('assets'); ?>/css/custom.css" rel="stylesheet">
<!--        <link href="<?php echo base_url('assets'); ?>/css/dev_styles.css" rel="stylesheet">-->
    </head>
    <body class="gray-bg">

<div class="middle-box loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name text-center"><img src="<?php echo base_url('assets').'/img/logo.png' ?>" width="150px"/></h1>
        </div>
        <h3 class="text-center">Welcome to Oye Deals Administration</h3>
        <form class="m-t" name="adminloginForm" id="adminloginForm" role="form" method="post" action="<?php echo base_url().'admin/login' ?>" >
            <div class="form-group">
                <input  type="text" id="admin_username" name="admin_username" placeholder="Admin Email"  class="form-control"/>

<!--                    <input type="email" class="form-control" placeholder="Username" required="">-->
            </div>
            <div class="form-group">
                <input name="admin_password" id="admin_password" type="password" class="form-control" placeholder="Password" >
            </div>
            <div class="form-group pull-left">
                <div class="i-checks"><label> <input type="checkbox" name="admin_remember"><i></i> Remember me </label></div>
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

           <a href="#forgotpass" data-backdrop="static" data-toggle="modal" data-keyboard="false"><small>Forgot password?</small></a>
        </form>
    </div>
</div>

<!-- Forgot password modal start -->
<div id="forgotpass" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Forgot Password</h4>
            </div>
            <div class="modal-body">
                <form id="forgotPassForm"  method="post" enctype="multipart/form-data" action="<?php echo base_url().'login/forgotpass' ?>">

                    <div>
                        <div id="name" class="tab-pane active">
                            <div class="panel-body">
                                <div class="ibox float-e-margins marg-bot10">

                                    <div class="ibox-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <center><div id="SuccessMessage" style="padding-bottom:10px;"></div></center>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                        <div class="col-md-12">
                                                            <label>Your Registered Email Address:</label>
                                                            <input type="text" id="emailid" name="emailid" class="form-control" value="" />
                                                            <span id="ErrorMessage" style="color:#FF0000"></span>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center marg-top15">
                        <button type="button" id="forgotPass" onclick="validateEmail()" class="btn btn-md btn-primary">Send</button>
                        <button type="button" id="cancel" class="btn btn-md btn-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Forgot password modal popup end-->
<script>
    $(document).ready(function () {
        //$(".chosen-select").select2();

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
                function validaeForm(frmName, rulesObj, messageObj, groupObj)
{
    
        // validate form
    $('#' + frmName).validate({
       
        ignore: '',
        onkeyup: false,
        errorClass: "text-error",
        validClass: "text-success",
        rules: rulesObj,
        messages: messageObj,
        groups: groupObj,
        invalidHandler: function (form, validator) {
        },
        showErrors: function (errorMap, errorList) {

            // create array of error list string

            var strElement = $.param(errorMap).split("&");



            // check error string is blank or not
            if ($.trim(strElement) != "")
            {
                // get id of first element
                ///following one line is added by keyur
                strElement[0] = unescape(strElement[0]);

                var arrName = strElement[0].split("=");

                // get element id

                var eleId = $('[name="' + arrName[0] + '"]').attr("id");

                // find tab div id of form element
                if (eleId == undefined)
                {///this condition is added by keyur
                    var parentId = $('[name="' + arrName[0] + '"]').parents('div[id]').parents('div[id]').attr("id");
                } else
                    var parentId = $("#" + eleId).parents('div[id]').attr("id");


                // active tab 
                $('a[href|="#' + parentId + '"]').tab("show");

            }

            // show default error message
            this.defaultShowErrors();
        }
    });
}

        $('form[name=adminloginForm]').submit(function(e) {
            console.log($('#adminloginForm').valid());
            //action="<?php echo base_url().'adminlogin/loginAuthentication' ?>"
            if($('#adminloginForm').valid()){
                return true;
            }
            return false;
        });

        //// Toaster Notification
       
       var rules = {
            username:{required:true},
            password:{required:true},
              }
        var messages = {
            username:{required:"Please Fill Username"},
            password:{required:"Please Fill Password"},
        }
         validaeForm('adminloginForm', rules, messages);     
            
    });

    
    function validateEmail()
    {
        //alert("<?php echo base_url(); ?>admin/admin/forgotpass");
        var email = document.getElementById("emailid").value;
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(email.length == 0)
        {
            document.getElementById("ErrorMessage").innerHTML = 'Please enter your registerd email id.';
        }
        else if(reg.test(email) == false)
        {
             document.getElementById("ErrorMessage").innerHTML = 'Please enter valid email id.';
        }
        else
        {
            document.getElementById("ErrorMessage").innerHTML = '';
            $.ajax(
                {
                    type:"post",
                    url: "<?php echo base_url(); ?>admin/admin/forgotpass",
                    data:{ email:email},
                    success:function(response)
                    {
                        if (response == 1)
                        {
                            document.getElementById("SuccessMessage").innerHTML = "<span style='color:#18a689'><h4>Please check your email to reset password link.</h4></span>";
                            setTimeout(function(){
                                $('#forgotpass').modal('hide');
                            }, 3000);
                        }
                        else
                        {
                            document.getElementById("SuccessMessage").innerHTML = "<span style='color:#FF0000'><h4>This email address is not registered in our system.</h4></span>";
                        }
                    }
                });
        }
    }
</script>


<!-- Mainly scripts -->

<script src="<?php echo base_url('assets'); ?>/js/jquery-ui-1.10.4.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/toastr/toastr.min.js"></script>

<!-- Jquery Validation -->
<script src="<?php echo base_url('assets'); ?>/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/jquery-validation/additional-methods.js"></script>
<!-- chosen -->
<script src="<?php echo base_url('assets'); ?>/js/plugins/chosen/chosen.jquery.js"></script>
<!-- select2 -->
<script src="<?php echo base_url('assets'); ?>/js/plugins/select2/select2.full.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets'); ?>/js/plugins/iCheck/icheck.min.js"></script>
</body>

</html>

