       <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        
                        <h5>Active Coupons</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><?php  echo isset($total_count['availableCount'])?$total_count['availableCount']:'';?></h1>
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                       
                        <h5>Grabbed Coupons</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><?php  echo isset($total_count['capturedCount'])?$total_count['capturedCount']:'';?></h1>
                       
                        
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                      
                        <h5>Redeemed Coupons</h5>
                    </div>
                    <div class="ibox-content">
                    <h1 class="no-margins"><?php  echo isset($total_count['reemeedCount'])?$total_count['reemeedCount']:'';?></h1>
                     </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                      
                        <h5>Expried Coupons</h5>
                    </div>
                    <div class="ibox-content">
                    <h1 class="no-margins"><?php  echo isset($total_count['expriedCount'])?$total_count['expriedCount']:'';?></h1>
                     </div>
                </div>
            </div>
        </div>
        <div class="row">

             <div class="col-lg-6">
                <div class="ibox float-e-margins dContainer">
                <div class="ibox-title">
                    <h5>Offers</h5>
                    <a class="btn pull-right btn-primary btn-sm" href="<?php echo base_url()."admin/offers"; ?>" >View All</a>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Offer Title</th>
                                    <th>Expiry Date</th>
                                   <th>status</th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php 
                               if(isset($all_offer)){
                                $cnt = count($all_offer)>5 ? 5 :count($all_offer); 
                                for($i=0 ; $i< $cnt; $i++) { ?>
                                <tr>
                                    <td><?php echo $all_offer[$i]['offer_title']; ?></td>
                                    <td><?php echo date('d-m-Y H:m',strtotime($all_offer[$i]['offer_enddate'])); ?></td>
                                    <td><?php echo ($all_offer[$i]['status'] != 0) ? 'Active' :'Inactive'; ?></td>
                                </tr>
                               <?php  } }?>
                            </tbody>
                        </table>
                  </div>

                 </div>
        </div>
             </div>
            <div class="col-lg-6">
                <div class="ibox float-e-margins dContainer">
                <div class="ibox-title">
                    <h5>Business Type</h5>
                    <a class="btn pull-right btn-primary btn-sm" href="<?php echo base_url()."admin/business_type"; ?>" >View All</a>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Nature of Business</th>
                                    <th>Created Date</th>
                                   <th>status</th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php 
                               if(isset($business_types)){
                                foreach ($business_types as  $value) { ?>
                                <tr>
                                    <td><?php echo $value->business_type_name; ?></td>
                                    <td><?php echo date('d-m-Y H:m',strtotime($value->created_date)); ?></td>
                                    <td><?php echo ($value->status != 0) ? 'Active' :'Inactive'; ?></td>
                                </tr>
                               <?php  } } ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            </div>

        </div>

        <div class="row">

        <div class="col-lg-12">
        <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Users</h5>
            
        </div>
        <div class="ibox-content">
          
            <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins dContainer">
                <div class="ibox-title">
                    <h5>App User</h5>
                    <a class="btn pull-right btn-primary btn-sm" href="<?php echo base_url()."admin/app_users"; ?>" >View All</a>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width:10%">Firstname</th>
                                    <th style="width:10%">Lastname</th>
                                    <th style="width:50%">Email</th>
                                    <th style="width:20%">Phone</th>
                                    <th style="width:10%">status</th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php 
                                if(isset($appuserlist)){
                               foreach ($appuserlist as  $val) {
                                    ?>
                                    <tr>
                                     
                                       <td><?php echo $val->first_name; ?></td>
                                        <td><?php echo $val->last_name; ?></td>
                                        <td><?php echo $val->email; ?></td>
                                        <td><?php echo $val->phone_number; ?></td>
                                        <td><?php echo ($val->status==1)?'Active':'Inactive'?> </td>
                                   
                                         </td>
                                    </tr>
                                <?php } }?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>


        <div class="col-lg-6">
            <div class="ibox float-e-margins dContainer">
                <div class="ibox-title">
                    <h5>Business User</h5>
                    <a class="btn btn-sm pull-right btn-primary" href="<?php echo base_url()."admin/business_users"; ?>" >View All</a>
                </div>
                <div class="ibox-content">
<!--                    <div class="row">
                        <div class="col-sm-12  m-b-xs">
                            <div data-toggle="buttons" class="btn-group pull-right">
                                <label class="btn btn-sm btn-white"> <input type="radio" id="option1" name="options"> Day </label>
                                <label class="btn btn-sm btn-white active"> <input type="radio" id="option2" name="options"> Week </label>
                                <label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options"> Month </label>
                            </div>
                        </div>
                    </div>-->
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover today_event">
                            <thead>
                                <tr>

                                    <th>Business Name</th>
                                    <th>Contact Person</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                               
                               <?php 
                                 if(isset($businessuserlist)){
                               foreach ($businessuserlist as  $val) {
                                    ?>
                                    <tr>
                                     
                                        <td><?php echo $val->business_name; ?></td>
                                        <td><?php echo $val->contact_person; ?></td>
                                        <td><?php echo $val->email_id; ?></td>
                                        <td><?php echo $val->phone; ?></td>
                                        <td><?php echo ($val->status==1)?'Active':'Inactive'?> </td>
                                    </tr>
                               <?php } } ?>
                             </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(window).resize();
//        var today_event = $('.today_event').DataTable({
//            "processing": true,
//            "serverSide": true,
//            "bFilter": true,
//            "order": [[0, "desc"]],
//            "ajax": {
//                url: "<?php echo base_url(); ?>dashboard/getTodayEventDataAjax",
//               // data: {'caseNo': $("#caseNo").val()},
//                type: "POST"
//            },
//         });
    });
</script>
 
        </div>

        </div>


        </div>
