<div class="wrapper wrapper-content">


    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>App Users</h5>
                    <!--                    <div class="ibox-tools"> <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#add"> Add</button> </div>-->
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-9 m-b-xs">
                            <input type="text" name="app_user_search" id="app_user_search" class="form-control input-sm" placeholder="Search" />

                        </div>

                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped" id="app_users_list" data-excelTitle="AppUsers" data-pdfTitle="AppUsers" data-filterKey="app_user_search">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($appuserlist as $key => $val) {
                                    ?>
                                    <tr>
                                        <td><?php echo $key + 1; ?></td>
                                        <td><?php echo $val['first_name']; ?></td>
                                        <td><?php echo $val['last_name']; ?></td>
                                        <td><?php echo $val['email']; ?></td>
                                        <td><?php echo $val['phone_number']; ?></td>
                                        <td>
    <!--                                            <a href="javascript:void(0);" onclick="return changeStatus(this, 'users', '<?php echo $val['status']; ?>');" data-field_name="user_id" data-id="<?php echo $val['user_id']; ?>"><?php echo $this->common_functions->displayStatus($val['status']); ?></a>-->
                                            <a href="javascript:void(0);" onclick="return changeStatus('change_app_user_status', '<?php echo $val['user_id']; ?>', '<?php echo $val['status']; ?>');"><?php echo $this->common_functions->displayStatus($val['status']); ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="add" role="dialog">
    <div class="modal-dialog"> 

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Nature Of Business</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form>
                        <div class="col-sm-10 col-sm-offset-1">

                            <div class="form-group">
                                <label >Nature Of Business</label>
                                <input type="text" class="form-control" required="" id="nature_bussiness" placeholder="Nature of Businees">
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <input type="radio" checked="" class="form-control" name="status_bussiness" id="status_bussiness" value="1"> Active
                                <input type="radio" class="form-control" name="status_bussiness" id="status_bussiness" value="0"> Inactive
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="reset">Reset </button>
                                <button class="btn btn-success" type="Submit" id="business_type_submit">Submit </button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
</script>