  <div class="wrapper wrapper-content">
      
      
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>Business Types</h5>
              <div class="ibox-tools"> <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#add_bus_type"> Add</button> </div>
            </div>
            <div class="ibox-content">
              <div class="row">
                <div class="col-sm-9 m-b-xs">
                  
                </div>
                
              </div>
              <div class="table-responsive">
                <table class="table table-striped" id="business_types">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Nature of Business  </th>
                      <th>Created Date</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $i=1;
                    foreach ($business_types as $key => $value) { ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $value['business_type_name']; ?></td>
                      <td><?php echo $value['created_date']; ?></td>
                      <td><?php echo ($value['status'] != 0) ? 'Active' :'Inactive'; ?></td>
                      <td><button id="<?php echo $value['business_type_id'];?>">Edit</button></td>
                    </tr>
                    <?php $i= $i+1;  } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
   

<div class="modal fade" id="add_bus_type" role="dialog">
<div class="modal-dialog"> 
  
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Add Nature Of Business</h4>
    </div>
    <div class="modal-body">
      <div class="row">
        <form>
          <div class="col-sm-10 col-sm-offset-1">
            
            <div class="form-group">
              <label >Nature Of Business</label>
              <input type="text" class="form-control" required="" id="nature_bussiness" placeholder="Nature of Businees">
            </div>
            <div class="form-group" style="padding: 10px 0 10px 0;">
              <label>Status</label>
              <input type="radio" checked class="" name="status_bussiness" id="status_bussiness_1" value="1"> <label for="status_bussiness_1" style="font-weight: normal;">Active</label>
              <input type="radio" class="" name="status_bussiness" id="status_bussiness_0" value="0"> <label for="status_bussiness_0" style="font-weight: normal;">Inactive</label>
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="reset">Reset </button>
                <button class="btn btn-success" type="Submit" mode="save" id="business_type_submit" upid="">Submit </button>
              </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>



