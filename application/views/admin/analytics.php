<div class="wrapper wrapper-content">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>Coupon Reports</h5>
            </div>
            <div class="ibox-content">
              <div class="row">
                <form id="analyticsForm" name="analyticsForm" method="post">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-md-3 col-lg-2 control-label">Coupon Type:</label>
                      <div class="col-md-3 col-lg-2">
                        <div class="i-checks">
                          <label>
                            <input type="checkbox" class="cpntype" name="coupontype[]" value="1">
                            <i></i> Active </label>
                          <br>
                          <label>
                            <input type="checkbox" class="cpntype" name="coupontype[]" value="3">
                            <i></i> Redeemed </label>
                        </div>
                      </div>
                      <div class="col-md-3 col-lg-2">
                        <div class="i-checks">
                          <label>
                            <input type="checkbox" class="cpntype" name="coupontype[]" value="2">
                            <i></i> Grabbed </label>
                          <br>
                          <label>
                            <input type="checkbox" class="cpntype" name="coupontype[]" value="4">
                            <i></i> Expired </label>
                        </div>
                      </div>
                        <label id="coupontype[]-error" class="text-error" for="coupontype[]"></label>
                        
                    </div>
                  </div>
                  <div class="col-md-12 top-pad">
                    <div class="form-group" id="data_5">
                      <label class="col-md-3 col-lg-2 control-label">Date Range:</label>
                      <div class="input-daterange input-group" id="datepicker">
                        <input type="text" class="input-sm form-control" id="start_date" name="start" placeholder="Start Date"/>
                        <span class="input-group-addon">to</span>
                        <input type="text" class="input-sm form-control" id="end_date" name="end" placeholder="End Date" />
                      </div>
                    </div>
                  </div>
                  <div class="col-md-10 col-md-offset-4 top-pad">
                    <div class="form-group">
                      <button class="btn btn-danger" id="" type="reset">Reset</button>
                      <input type="submit" class="btn btn-primary" id="analytics_submit" type="button" value="Submit">
                    </div>
                  </div>
                </form>
                <div class="col-md-12 top-pad">
                  <div class="table-responsive col-sm-10 col-sm-offset-1">
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<script>
     $(document).ready(function(){
        var rules = {
            start: {required: true},
            end: {required: true},
            "coupontype[]":{required: true}
              }
        var messages = {
            start: {
                required: "Please select start date"
            },
            end: {
                required: "Please select end date",
           },
            'coupontype[]': {
                required: "You must check at least 1 box",
            }    
        };
         validaeForm('analyticsForm', rules, messages);
    });
</script>