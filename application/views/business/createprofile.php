<!DOCTYPE html>
<?php //echo '<pre>'; print_r($business_type); exit ?>
<html>
<head>
<title>Create Business Profile</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<link href="<?php echo base_url('assets'); ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/plugins/dropzone/basic.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/plugins/dropzone/dropzone.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/custom.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/plugins/dropzone/basic.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/plugins/dropzone/dropzone.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/plugins/codemirror/codemirror.css" rel="stylesheet">
</head>

<body class="gray-bg">
<div class="middle-box text-center loginscreen animated fadeInDown">
  <div>
    <h2>Create Business Profile</h2>
    <hr>

    <form class="m-t" role="form" method="post" id="registerForm" name="registerForm" enctype="multipart/form-data" action="<?php  echo base_url().'business/updateprofile' ?>">
      <div class="form-group">
        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
          <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span> </div>
          <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Upload Logo</span> <span class="fileinput-exists">Change</span>
          <input type="file" id="imgInp" name="profile" onchange="readURL(this);"/>
          </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
          <img src="" id="blah" class="logo1" />

          <p><br>*<?php echo $this->session->userdata('business_user_data')['BusinessUserName']; ?>*</p>
      </div>
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Address 1" id="address1" name="address1">
      </div>
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Address 2" id="address2" name="address2">
      </div>
        <!--input id="geocomplete" type="text" placeholder="Type in an address" value="" /-->
       <div class="form-group">
        <input type="text" class="form-control" id="location" name="location" placeholder="Location">
      </div>
      <div class="form-group">
        <input type="text" class="form-control" id="area" data-geo="sublocality" readonly name="area" placeholder="Area">
      </div>
      <div class="form-group">
        <input type="text" class="form-control" data-geo="locality" id="city" name="city" readonly placeholder="City">
      </div>
      <div class="form-group">
        <input type="text" class="form-control" data-geo="administrative_area_level_1" readonly id="state" name="state" placeholder="State">
      </div>
      <div class="form-group">
        <input type="text" class="form-control" data-geo="country" id="country" readonly name="country" placeholder="Country">
      </div>


      <div class="form-group">
        <input type="text" class="form-control" data-geo="postal_code" name="zipcode" id="zipcode" placeholder="Pin Code">
      </div>
      <div class="form-group">
        <select class="form-control m-b" name="account" id="account">
          <option>-- Nature of Business --</option>
          <?php
          foreach($business_type as $key=>$value)
          {
          ?>
          <option value="<?php echo $value['business_type_id']; ?>"><?php echo $value['business_type_name']; ?></option>
         <?php
          }
         ?>
        </select>
      </div>
      <div class="form-group">
        <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number">
      </div>
      <div class="form-group">
        <input type="text" class="form-control" id="contact_perosn" name="contact_perosn" placeholder="Contact Person Name">
      </div>
      <input type="submit" class="btn btn-primary" value="Submit">
    </form>
  </div>
</div>

<!-- Mainly scripts -->
<script src="<?php echo base_url('assets'); ?>/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets'); ?>/js/plugins/iCheck/icheck.min.js"></script>
<script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/dropzone/dropzone.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/codemirror/codemirror.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/codemirror/mode/xml/xml.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAKf3mILelPJRBL8ksWrskqVRJ623Wy9Fo"></script>
<script src="<?php echo base_url('assets'); ?>/js/jquery.geocomplete.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/jquery-ui-1.10.4.min.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins/jquery-validation/jquery.validate.min.js"></script>

<script>

        $(document).ready(function(){

            Dropzone.options.myAwesomeDropzone = {

                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 100,
                maxFiles: 100,

                // Dropzone settings
                init: function() {
                    var myDropzone = this;

                    this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        myDropzone.processQueue();
                    });
                    this.on("sendingmultiple", function() {
                    });
                    this.on("successmultiple", function(files, response) {
                    });
                    this.on("errormultiple", function(files, response) {
                    });
                }

            }
               function validaeForm(frmName, rulesObj, messageObj, groupObj)
{
    // validate form
    $('#' + frmName).validate({
        ignore: '',
        onkeyup: false,
        errorClass: "text-error",
        validClass: "text-success",
        rules: rulesObj,
        messages: messageObj,
        groups: groupObj,
        invalidHandler: function (form, validator) {
        },
        showErrors: function (errorMap, errorList) {

            // create array of error list string

            var strElement = $.param(errorMap).split("&");



            // check error string is blank or not
            if ($.trim(strElement) != "")
            {
                // get id of first element
                ///following one line is added by keyur
                strElement[0] = unescape(strElement[0]);

                var arrName = strElement[0].split("=");

                // get element id

                var eleId = $('[name="' + arrName[0] + '"]').attr("id");

                // find tab div id of form element
                if (eleId == undefined)
                {///this condition is added by keyur
                    var parentId = $('[name="' + arrName[0] + '"]').parents('div[id]').parents('div[id]').attr("id");
                } else
                    var parentId = $("#" + eleId).parents('div[id]').attr("id");


                // active tab
                $('a[href|="#' + parentId + '"]').tab("show");

            }

            // show default error message
            this.defaultShowErrors();
        }
    });
}
        var rules = {
            address1: {required: true},
            address2: {required: true},
            location: {required: true},
            zipcode: {required: true},
            account: {required: true},
            phone: {required: true,
                    maxlength:10,
                    digit:true},
            contact_perosn: {required: true},
           // pwd: {required: true, whitespaceValue: true,minlength:5 /*passwordValue: true*/},
          //  pwd1:{required: true, whitespaceValue: true,minlength:5 /*passwordValue: true*/},
        }
        var messages = {
            address1: {
                required: "Please enter address"
            },
            address2: {
                required: "Please enter address"
            },
            location: {
                required: "Please enter location"
            },
            zipcode:{
                required: "Please enter zipcode"
            },

            account: {
                required: "Please select nature of business"
            },
            phone:{
                required: "Please enter phone"
            },
            contact_perosn:{
              required: "Please enter contact person name"
            },



          /*  pwd: {
                required: "Please enter password",
                minlength: "Please enter minimum 5 character password",
                whitespaceValue: "White Space is not allowed for password",
                //passwordValue: "Password must have at least one uppercase letter, one digits and one special character"
            }*/
        }


         validaeForm('registerForm', rules, messages);
    });
    $('#signin.modal').on('hidden.bs.modal', function () {
                $(this).find('input,textarea,select').each(function () {

                   // $(this).trigger('change');
                });
            });
     $('#signup.modal').on('hidden.bs.modal', function () {
                $(this).find('input,textarea,select').each(function () {

                   // $(this).trigger('change');
                });
            });



        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
</body>
</html>

<script>
$(function(){
        $("#location").geocomplete({
          details: "form .form-group",
          detailsAttribute: "data-geo"
        });

        $("#find").click(function(){
          $("#location").trigger("geocode");
        });
      });
</script>
