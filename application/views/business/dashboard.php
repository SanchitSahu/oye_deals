<?php //echo '<pre>'; print_r($this->session->userdata('BusinessUserId')); exit; ?>
<div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom">
      <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header"> <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
          <form role="search" class="navbar-form-custom" action="search_results.html">
<!--            <div class="form-group">
              <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
            </div>-->
          </form>
        </div>
      </nav>
    </div>
    <div class="wrapper wrapper-content">
      <div class="row">
        <div class="col-lg-3">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>Coupons Active</h5>
            </div>
            <div class="ibox-content">
              <h1 class="no-margins"><?php echo $this->common_functions->get_activecouponcount(); ?></h1>
            </div>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>Coupons Grabbed</h5>
            </div>
            <div class="ibox-content">
              <h1 class="no-margins"><?php echo $this->common_functions->get_grabedcouponcount()+ $this->common_functions->get_redeemcouponcount(); ?></h1>
            </div>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>Coupons Redeemed</h5>
            </div>
            <div class="ibox-content">
              <h1 class="no-margins"><?php echo $this->common_functions->get_redeemcouponcount(); ?></h1>
            </div>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>Coupons Expired</h5>
            </div>
            <div class="ibox-content">
              <h1 class="no-margins"><?php echo $this->common_functions->get_expiredcouponcount(); ?></h1>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>Redeem Now</h5>
              <div class="ibox-tools"> <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a></div>
            </div>
            <div class="ibox-content">
              <div class="row">
                <div class="col-sm-3">
                  <div class="input-group">
                    <input type="text" placeholder="Search" class="input-sm form-control">
                    <span class="input-group-btn">
                    <button type="button" class="btn btn-sm btn-primary"> Go!</button>
                    </span></div>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name </th>
                      <th>Contact </th>
                      <th>Company </th>
                      <th>Coupon </th>
                      <th> </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Patrick Smith</td>
                      <td>0800 051213</td>
                      <td>Domino's</td>
                      <td>EKLA76L4SKJD</td>
                      <td><button type="button" class="btn btn-sm btn-primary"> Redeem Now!</button></td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Henry Jones</td>
                      <td>0900 054215</td>
                      <td>Apna Adda</td>
                      <td>ASU9876ODH</td>
                      <td><button type="button" class="btn btn-sm btn-primary"> Redeem Now!</button></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


