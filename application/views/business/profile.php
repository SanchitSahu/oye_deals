<link href="<?php echo base_url('assets'); ?>/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/plugins/codemirror/codemirror.css" rel="stylesheet">
<div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header"> <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                <form role="search" class="navbar-form-custom" action="search_results.html">
<!--                    <div class="form-group">
                        <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                    </div>-->
                </form>
            </div>
        </nav>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Edit Profile</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <form class="m-t" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url().'business/updateprofile' ?>">
                                    <div class="form-group" align="center">
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span> </div>
                                            <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Change Logo</span> <span class="fileinput-exists">Change</span>
                                                <input type="file" id="imgInp" name="profile" onchange="readURL(this);"/>
                                            </span> <a href="" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                                        <?php
                                            if(!empty($profile->logo))
                                            {
                                        ?>
                                            <img src="<?php echo base_url().'assets/profileimages/'.$profile->logo; ?>" id="blah" class="logo1" />
                                        <?php
                                            }
                                            else
                                            {
                                        ?>
                                            <img src="" id="blah" class="logo1" />
                                        <?php
                                            }

                                        ?>

                                        <p><br>
                                            *<?php echo $this->session->userdata('business_user_data')['BusinessUserName']; ?>*</p>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Address 1" id="address1" value="<?php if(isset($profile->address_1)){echo $profile->address_1;} ?>" name="address1">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Address 2" id="address2" value="<?php if(isset($profile->address_2)){echo $profile->address_2;} ?>" name="address2">
                                    </div>
                                    <!--input id="geocomplete" type="text" placeholder="Type in an address" value="" /-->
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="location" value="<?php if(isset($profile->location)){echo $profile->location;} ?>" name="location" placeholder="Location">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="area" data-geo="sublocality" readonly name="area" value="<?php if(isset($profile->area)){echo $this->common_functions->get_areaname($profile->area);}  ?>" placeholder="Area">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" data-geo="locality" id="city" name="city" readonly value="<?php if(isset($profile->city)){echo $this->common_functions->get_cityname($profile->city);}  ?>" placeholder="City">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" data-geo="administrative_area_level_1" value="<?php if(isset($profile->state)){echo $this->common_functions->get_statename($profile->state);}  ?>" readonly id="state" name="state" placeholder="State">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" data-geo="country" id="country" readonly value="<?php if(isset($profile->country)){echo $this->common_functions->get_countryname($profile->country);}  ?>" name="country" placeholder="Country">
                                    </div>


                                    <div class="form-group">
                                        <input type="text" class="form-control" data-geo="postal_code" name="zipcode" value="<?php if(isset($profile->pincode)){echo $profile->pincode;} ?>" id="zipcode" placeholder="Pin Code">
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control m-b" name="account" id="account">
                                            <option>-- Nature of Business --</option>
                                            <?php
                                            foreach ($business_type as $key => $value) {
                                                ?>
                                                <option value="<?php echo $value['business_type_id']; ?>" <?php if($value['business_type_id'] == $profile->bussiness_type) { echo 'selected'; } ?>><?php echo $value['business_type_name']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="phone" id="phone" maxlength="10" value="<?php if(isset($profile->phone)){echo $profile->phone;} ?>" placeholder="Phone Number">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="contact_perosn" name="contact_perosn" value="<?php if(isset($profile->contact_person)){echo $profile->contact_person;} ?>" placeholder="Contact Person Name">
                                    </div>
                                    <div align="center">
                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
      function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function(){
        $("#location").geocomplete({
          details: "form .form-group",
          detailsAttribute: "data-geo"
        });

        $("#find").click(function(){
          $("#location").trigger("geocode");
        });
      });
    </script>