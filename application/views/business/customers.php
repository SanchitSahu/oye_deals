<div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom">
      <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header"> <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
          <form role="search" class="navbar-form-custom" action="search_results.html">
<!--            <div class="form-group">
              <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
            </div>-->
          </form>
        </div>
      </nav>
    </div>
    <div class="wrapper wrapper-content">
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>Customers</h5>
            </div>
            <div class="ibox-content">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Sr No</th>
                      <th>Name </th>
                      <th>Email </th>
                      <th>Grabbed </th>
                      <th>Redeemed </th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                        $i = 1;
                        foreach($users as $key=>$value)
                        {
                      ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $value->first_name." ".$value->last_name ?></td>
                       <td><?php echo $value->email." ".$value->last_name ?></td>
                      <td><?php echo $this->common_functions->grabbedcount($value->user_id)+$this->common_functions->redeemedcount($value->user_id);  ?></td>
                      <td><?php echo $this->common_functions->redeemedcount($value->user_id);  ?></td>
                    </tr>
                    <?php
                            $i++;
                        }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
