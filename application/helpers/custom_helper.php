<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#Check unique Token

function getUniqueToken($table_name, $token_column = '', $condition = '', $token_length = 10, $token_type = 'small_alpha_numeric') {
    $token_column = !empty($token_column) ? $token_column : 'token';

//loop to generate random token
    while (true) {
//generate random token
        $token = generateRandomString($token_length, $token_type);
        if (count(filterTableData($table_name, $token_column, array('auth_token' => $token))) == 0) {
            break;
        }
    }

    return $token;
}

# Genereate random string

function generateRandomString($length = 10, $string_type = 'all') {
    $random_string = "";
    $all_chars = "";


    if ($string_type == 'small_alpha') {
        $all_chars = "abcdefghijklmnopqrstuvwxyz";
    } else if ($string_type == 'caps_alpha') {
        $all_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    } else if ($string_type == 'small_alpha_numeric') {
        $all_chars = "0123456789abcdefghijklmnopqrstuvwxyz";
    } else if ($string_type == 'caps_alpha_numeric') {
        $all_chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    } else if ($string_type == 'alpha_numeric') {
        $all_chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    } else if ($string_type == 'numeric') {
        $all_chars = "0123456789";
    }


    if ($string_type != 'all') {
//loop to generate string
        for ($i = 0; $i < $length; $i++) {
            $random_string .= $all_chars[rand(0, strlen($all_chars) - 1)];
        }
    }


//if none of the type found the consider default type and use all characters
//for password this will be used
    if ($string_type == 'all') {
        $all_part = substr(str_shuffle("*^%#@!0123456789abcdefghijklmno*^%#@!qrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length - 3);
        $caps_part = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1);
        $number_part = substr(str_shuffle("0123456789"), 0, 1);
        $special_part = substr(str_shuffle("*^%#@!"), 0, 1);

        $random_string = $number_part . $all_part . $caps_part . $special_part;
    }

    return $random_string;
}

#filter table data

function filterTableData($table_name, $column, $condition) {
    $ci = get_instance();
    $ci->db->select($column);
    $ci->db->from($table_name);
    $ci->db->where($condition);
    $query = $ci->db->get();
    return $query->result();
}

#upload profile picture

function uploadProfilePicture($file, $details = '') {
    $ci = get_instance();
    $_FILES = $file;
    $config['upload_path'] = './assets/profilepic/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = 100000;
    $config['max_width'] = 2048;
    $config['max_height'] = 2048;
    $ci->load->library("upload");
    $ci->upload->initialize($config);
    if (!$ci->upload->do_upload('profile_pic')) {
        $error = array('error' => $ci->upload->display_errors());
        return $error;
    } else {
        $data = $ci->upload->data();
//        if ($details && $details['profilepicture']) {
//            unlink('./assets/profilepicture/' . $details['profilepicture']);
//        }
        return $data['file_name'];
    }
}

function send_push($deviceid = '', $message = '', $details = '') {
    if (is_array($deviceid)) {
        $registration_ids = $deviceid;
    } else {
        $registration_ids = array($deviceid);
    }
    $fields = array(
        'registration_ids' => $registration_ids,
        'data' => array('title' => 'Notification from OyeDeals', 'message' => $message),
        'notification' => array('title' => 'Notification from OyeDeals', 'body' => $message),
        "content_available" => true,
        "priority" => "high"
    );
    $headers = array(
//        'Authorization: key=AAAAqgiLF-s:APA91bFqCBipGUh9g9K-x-NMVZc4mphdA2_nC1fD73kKI7bXgkx2tlXScYWa2sZu0NGMe2we7K5e5cMfid46_PO24OTbrIOFD2VBsffLGInEp-1ICap0gMwvw5mxzWskesziQwf2UKpf', // FIREBASE_API_KEY_FOR_ANDROID_NOTIFICATION
        'Authorization: key=AIzaSyChuSthcjQhysJQTJhfLOsD3br0Dn30mQk', // FIREBASE_API_KEY_FOR_ANDROID_NOTIFICATION
        'Content-Type: application/json'
    );

//    echo "<pre>";print_r($headers);
//    echo "<pre>";print_r($fields);exit;
// Open connection
    $ch = curl_init();
// Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
// Execute post
    $result = curl_exec($ch);
    if ($result === false) {
        die('Curl failed:' . curl_errno($ch));
    }
// Close connection
    curl_close($ch);
//    print_r($result);
//    die;
    return $result;
}
