<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Social_login_model extends CI_Model {

    public function Social_login_model() {
        parent::__construct();
        $this->table_name = 'social_login';
    }

    public function select($select = '', $where = '', $type = '', $join = '', $orderby = '', $limit = '') {
        $this->db->select($select);
        $this->db->from($this->table_name);
        if ($where) {
            $this->db->where($where);
        }
        if ($join) {
            $this->db->join($join);
        }
        if ($orderby) {
            $this->db->order_by($orderby, 'DESC');
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        if ($type == 'A') {
            return $query->result_array();
        } else
        if ($type == 'R') {
            return $query->row();
        } else {
            return $query->result();
        }
    }

    public function insert($insert) {
        $result = $this->db->insert($this->table_name, $insert);
        $id = $this->db->insert_id();
        return $id;
    }

    public function update($update, $where) {
        $this->db->where($where);
        $result = $this->db->update($this->table_name, $update);
        return $result;
    }

    public function delete($delete) {
        $this->db->where($delete);
        $result = $this->db->delete($this->table_name);
        return $result;
    }

}
