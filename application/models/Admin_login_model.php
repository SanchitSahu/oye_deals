<?php

/**
 * Bhanushankr Joshi @ Solulab
 * Started : 15-09-2017
 */
class Admin_login_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * resolve_admin_user_login function.
     *
     * @access public
     * @param mixed $username
     * @param mixed $password
     * @return bool true on success, false on failure
     */
    public function resolve_admin_user_login($username, $password) {
        $this->db->select('id,email,name,password,profilepic,created_date,updated_date');
        $this->db->from('superadmin');
        $this->db->where('email', $username);
        $this->db->where('password', MD5($password));
        //$this->db->get('superadmin sa');
        $hash = $this->db->get()->row_array();
        //echo $this->db->last_query();exit;
        return $hash;
    }

    public function check_email($email) {
        $this->db->where('email', $email);
        $this->db->select('name');
        $q = $this->db->get('superadmin');
        $result = $q->row_array();
        $cnt = count($result);
        if (!empty($cnt)) {
            //$token = bin2hex(random_bytes(16));
            $token = md5(uniqid(rand(), true));
            $to = $email;
            //$this->common_functions->reg_send('dinky@solulab.com','Registration email','notification',$body);
            $link = "<a target='_blank' href='" . base_url() . "admin/admin/resetpass/$token'>Link</a>";
            $body = "Hello " . $result['username'] . ",<br/>
                        Please click on link to reset your password : $link<br/>
                        Thanks";

            $this->common_functions->reg_send($to, 'Reset Password', $result['username'], $body);

            $actData = array('fpassstr' => $token);
            $this->db->where('email', $email);
            $this->db->update('superadmin', $actData);
        }
        return $result;
    }

    public function check_fstr($fstr) {
        $this->db->where('fpassstr', $fstr);
        $this->db->select('sa_email');
        $q = $this->db->get('superadmin');
        return $q->row_array();
    }

    public function change_pass($fstr, $pass) {
        $record = array('password' => md5($pass), 'fpassstr' => '');
        $this->db->where('fpassstr', $fstr);
        $this->db->update('superadmin', $record);
    }

    public function check_pass($cond) {
        $this->db->where($cond);
        $this->db->select('email');
        $q = $this->db->get('superadmin');
        //echo $this->db->last_query(); exit;
        return $result = $q->row_array();
    }

    public function change_password($pass) {
        $initial = $this->session->userdata('admin_user_data')['email'];
        $record = array('password' => md5($pass));
        $this->db->where('email', $initial);
        $this->db->update('superadmin', $record);

        $arr = $this->session->userdata('admin_user_data');
        $arr['password'] = md5($pass);
        $this->session->set_userdata('admin_user_data', $arr);
        //echo $this->db->last_query(); exit;
    }

    public function add_offer_type($postdata) {
        if ($this->db->insert('offer_type', $postdata)) {
            return 1;
        } else {
            return 0;
        }
    }

}

?>