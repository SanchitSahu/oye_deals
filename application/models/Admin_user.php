<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Admin_user extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'adminusers';
        $this->load->model('common_model');
    }

    public function select($select = '', $where = '', $type = '', $join = '', $orderby = '', $limit = '') {
        $this->db->select($select);
        $this->db->from($this->table_name);
        if ($where) {
            $this->db->where($where);
        }
        if ($join) {
            $this->db->join($join);
        }
        if ($orderby) {
            $this->db->order_by($orderby, 'DESC');
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        // echo $this->db->last_query(); exit;
        if ($type == 'A') {
            return $query->result_array();
        } else
        if ($type == 'R') {
            return $query->row();
        } else {
            return $query->result();
        }
    }

    public function insert_data($insert) {
        $result = $this->db->insert($this->table_name, $insert);
        //echo $this->db->last_query(); exit;
        $id = $this->db->insert_id();
        return $id;
    }

    public function update($update, $where) {
        $this->db->where($where);
        $result = $this->db->update($this->table_name, $update);
        return $result;
    }

    public function delete($delete) {
        $this->db->where($delete);
        $result = $this->db->delete($this->table_name);
        return $result;
    }

    public function updateProfile($data, $files) {

        $countryid = $this->common_model->getcountryid($data['country']);
        $stateid = $this->common_model->getstateid($data['state'], $countryid);
        $cityid = $this->common_model->getcityid($data['city'], $stateid, $countryid);
        $areaid = $this->common_model->getareaid($data['area'], $cityid);
        $newUser = array(
            "address_1" => $data['address1'],
            "address_2" => $data['address1'],
            "location" => $data['location'],
            "country" => $countryid,
            "state" => $stateid,
            "city" => $cityid,
            "area" => $areaid,
            "pincode" => $data['zipcode'],
            "bussiness_type" => $data['account'],
            "phone" => $data['phone'],
            "contact_person" => $data['contact_perosn'],
        );

        // $userId = $this->session->userdata('business_user_data')['BusinessUserId'];
//        if($this->session->userdata('business_user_data') != null && $this->session->userdata('business_user_data') != "") {
        $userId = $this->session->userdata('business_user_data')['BusinessUserId'];
//        } else {
//            $userId = $this->session->userdata()['BusinessUserId'];
//        }

        $where = array('business_id' => $userId);
        $this->db->where($where);
        $this->db->update('adminusers', $newUser);


        if ($_FILES['profile']['name'] != "") {
            $this->load->library('upload');
            $nwfile = explode(".", $_FILES["profile"]['name']);
            $ext = end($nwfile);
            if (!is_dir(FCPATH . 'assets/profileimages')) {
                mkdir(FCPATH . 'assets/profileimages', 0777);
            }
            $config['upload_path'] = './assets/profileimages/'; //Use relative or absolute path

            $new_name = time() . "." . $ext;
            $config['allowed_types'] = 'jpg|png|jpeg|bmp';
            $config['file_name'] = $new_name;
            $config['max_size'] = '2048000'; // Can be set to particular file size , here it is 2 MB(2048 Kb)
            $config['max_width'] = '1024';
            $config['max_height'] = '768';
            $config['overwrite'] = FALSE;
            //If the file exists it will be saved with a progressive number appended
            //Initialize
            $this->upload->initialize($config);
            if (!$this->upload->do_upload("profile")) {
                $this->upload->display_errors();
                exit;
            } else {
                //echo 'in else';
                $record = array('logo' => $new_name);
                $this->db->where($where);
                $this->db->update('adminusers', $record);
            }
        }

        // echo 'last qry => '.$this->db->last_query(); exit;
        return true;
    }

    public function getAppUsers() {
        $this->db->select("*");
        $this->db->from('users');
        $query = $this->db->get();
        $users = $query->result_array();

        return $users;
    }

    public function getBusinessUsers() {
        $this->db->select("*");
        $this->db->from('adminusers');
        $query = $this->db->get();
        $users = $query->result_array();

        return $users;
    }

    public function getProfile() {
        //echo '<pre>'; print_r($this->session->userdata()['BusinessUserId']);
//        if ($this->session->userdata('business_user_data') != null && $this->session->userdata('business_user_data') != "") {
        $userId = $this->session->userdata('business_user_data')['BusinessUserId'];
//        } else {
//            $userId = $this->session->userdata()['BusinessUserId'];
//        }
        //$userId = isset($this->session->userdata('business_user_data')) ? $this->session->userdata('business_user_data')['BusinessUserId'] : $this->session->userdata()['BusinessUserId'];
        $where = array('business_id' => $userId);

        $this->db->select("*");
        $this->db->from('adminusers');
        $this->db->where($where);
        $query = $this->db->get();
        //echo 'last qry => '.$this->db->last_query(); exit;
        return $query->row();
    }

    public function save_offer_details_model($insert_params) {
        if ($this->db->insert('offers', $insert_params)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function save_offer_coupon_model($offer_id, $coupon, $latitude, $longitude) {
        $this->load->library('ciqrcode');

        $insert_params = array(
            'offer_id' => $offer_id,
            "coupon_code" => $coupon['CouponCode'],
            "latitude" => $latitude,
            "longitude" => $longitude,
            "created_date" => date('Y-m-d H:i:s'),
            "updated_date" => date('Y-m-d H:i:s')
        );
        if ($this->db->insert('offer_coupons', $insert_params)) {
            $coupon_id = $this->db->insert_id();
            $params['data'] = $coupon['CouponCode'];
            $params['savename'] = FCPATH . "assets/qr_codes/$coupon_id.png";
            $this->ciqrcode->generate($params);
            if ($this->db->update('offer_coupons', array("coupon_qr_code" => "$coupon_id.png"), array("offer_coupon_id" => $coupon_id))) {
                $coupon_id = $this->db->insert_id();
                $this->load->library('ciqrcode');

                $params['data'] = $coupon['CouponCode'];
                $params['savename'] = FCPATH . "assets/qr_codes/$coupon_id.png";
                $this->ciqrcode->generate($params);

                return $coupon_id;
            } else {
                return false;
            }
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    public function count_coupon()
    {
         $query = $this->db->query("
            SELECT 
            (SELECT COUNT(*) FROM offer_coupons WHERE  `status`='1') AS availableCount,
            (SELECT COUNT(*) FROM offer_coupons WHERE  `status`='2') AS capturedCount,
            (SELECT COUNT(*) FROM offer_coupons WHERE  `status`='3') AS reemeedCount,
            (SELECT COUNT(*) FROM offer_coupons WHERE  `status`='4') AS expriedCount
        ");
        return $query->result_array()[0];
    }

}
