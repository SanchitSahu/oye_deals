<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Common_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getcountryid($countryname) {
        $this->db->where('country_name', $countryname);
        $this->db->select('country_id');
        $res = $this->db->get('countries');
        $result = $res->result();

        if (count($result) > 0) {
            return $result[0]->country_id;
        } else {
            $insert = array(
                "country_name" => $countryname,
            );
            $insert = $this->db->insert('countries', $insert);
            return $country_id = $this->db->insert_id();
        }
    }

    public function getstateid($statename, $countryid) {
        $this->db->where('state_name', $statename);
        $this->db->select('state_id');
        $res = $this->db->get('states');
        $result = $res->result();
        if (count($result) > 0) {
            return $result[0]->state_id;
        } else {
            $insert = array(
                "state_name" => $statename,
                "country_id" => $countryid
            );
            $insert = $this->db->insert('states', $insert);
            return $state_id = $this->db->insert_id();
        }
    }

    public function getcityid($cityname, $stateid, $countryid) {
        $this->db->where('city_name', $cityname);
        $this->db->select('city_id');
        $res = $this->db->get('cities');
        $result = $res->result();
        //echo $this->db->last_query(); exit;
        if (count($result) > 0) {
            return $result[0]->city_id;
        } else {
            $insert = array(
                "city_name" => $cityname,
                "country_id" => $countryid,
                "state_id" => $stateid
            );
            $insert = $this->db->insert('cities', $insert);
            return $city_id = $this->db->insert_id();
        }
    }

    public function getareaid($areaname, $cityid) {
        $this->db->where('area_name', $areaname);
        $this->db->select('area_id');
        $res = $this->db->get('area');
        $result = $res->result();
        //echo $this->db->last_query(); exit;
        if (count($result) > 0) {
            return $result[0]->area_id;
        } else {
            $insert = array(
                "area_name" => $areaname,
                "city_id" => $cityid,
            );
            $insert = $this->db->insert('area', $insert);
            return $area_id = $this->db->insert_id();
        }
    }

    public function get_offer_data($offer_id) {
        $this->db->from('offers');
        $this->db->where('offer_id', $offer_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_offer_coupon_data($offer_id) {
        $this->db->select("oc.*, concat(u.first_name, ' ', u.last_name) as username, u.user_id");
        $this->db->from('offer_coupons oc');
        $this->db->where('oc.offer_id', $offer_id);
        $this->db->join('users u', 'u.user_id = oc.assigned_to', 'left');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function grab_offer_types($select = '') {
        $this->db->select($select);
        $this->db->from('offer_type');
        $this->db->where('status', 1);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function grab_all_offer() {
        $this->db->select('au.business_name, o.*');
        $this->db->from('offers o');
        $this->db->join('adminusers au', 'au.business_id = o.business_id', 'left');
        $this->db->group_by('o.offer_id');
        $this->db->where('o.status', 1);
        $this->db->order_by('o.created_date', 'desc');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function grab_avail_cap_coupon_count($offer_id) {
        $query = $this->db->query("
            SELECT
            (SELECT COUNT(*) FROM offer_coupons WHERE offer_id=$offer_id AND `status`='1') AS availableCount,
            (SELECT COUNT(*) FROM offer_coupons WHERE offer_id=$offer_id AND `status`='2') AS capturedCount
        ");
        return $query->result_array()[0];
    }

    public function grab_analytics_coupon($couponstatus = '', $start_date = '', $end_date = '') {
        if ($couponstatus != '') {
            $availableCount = '';
            asort($couponstatus);
            count($couponstatus);
            if (count($couponstatus) > 0) {
                foreach ($couponstatus as $key => $value) {
                    $availableCount .= "(SELECT COUNT(*) FROM offer_coupons WHERE  status='$value' AND created_date BETWEEN'" . $start_date . "' AND '" . $end_date . "') AS coupon_$value,";
                }
                $availableCount = rtrim($availableCount, ',');
            }
        }
        $query = $this->db->query("SELECT $availableCount ");
        return $query->result_array();
    }

}
