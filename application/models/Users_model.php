<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Users_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'users';
    }

    public function select($select = '', $where = '', $type = '', $join = '', $orderby = '', $limit = '') {
        $this->db->select($select);
        $this->db->from($this->table_name);
        if ($where) {
            $this->db->where($where);
        }
        if ($join) {
            $this->db->join($join);
        }
        if ($orderby) {
            $this->db->order_by($orderby, 'DESC');
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        if ($type == 'A') {
            return $query->result_array();
        } else
        if ($type == 'R') {
            return $query->row();
        } else {
            return $query->result();
        }
    }

    public function insert($insert) {
        $result = $this->db->insert($this->table_name, $insert);
        $id = $this->db->insert_id();
        return $id;
    }

    public function update($update, $where) {
        $this->db->where($where);
        $result = $this->db->update($this->table_name, $update);
        return $result;
    }

    public function delete($delete) {
        $this->db->where($delete);
        $result = $this->db->delete($this->table_name);
        return $result;
    }

    public function selectcustomer() {
        // $userid = $this->session->userdata('BusinessUserId');
//         if($this->session->userdata('business_user_data') != null && $this->session->userdata('business_user_data') != "") {
        $userid = $this->session->userdata('business_user_data')['BusinessUserId'];
//        } else {
//            $userid = $this->session->userdata()['BusinessUserId'];
//        }
        $this->db->where('business_id', $userid);
        $this->db->select('*');
        $this->db->from('offer_user_mapping AS mapp'); // I use aliasing make joins easier
        $this->db->join('users AS u', 'mapp.user_id = u.user_id', 'INNER');
        $this->db->join('offers AS o', 'o.offer_id = mapp.offer_id', 'INNER');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result();
    }

}
