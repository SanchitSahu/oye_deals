<?php

/**
 * Bhanushankr Joshi @ Solulab
 * Started : 15-09-2017
 */
class Business_type extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'business_type';
    }

    public function add_business_type($postdata) {
        if ($this->db->insert('business_type', $postdata)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function grab_business_types($select = '', $id = '', $order = '', $status = '') {
        $this->db->select($select);
        $this->db->from($this->table_name);
        if ($id != '') {
            $this->db->where('business_type_id', $id);
        }
        if ($status != '') {
            $this->db->where('status', $status);
        }
        if ($order != '') {
            $this->db->order_by($order);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update_business_type($postdata, $where) {
        $this->db->where($where);
        $result = $this->db->update($this->table_name, $postdata);
        //echo $this->db->last_query(); exit;
        return $result;
    }

    public function select($select = '', $where = '', $type = '', $join = '', $orderby = '', $limit = '') {
        $this->db->select($select);
        $this->db->from($this->table_name);
        if ($where) {
            $this->db->where($where);
        }
        if ($join) {
            $this->db->join($join);
        }
        if ($orderby) {
            $this->db->order_by($orderby, 'DESC');
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        if ($type == 'A') {
            return $query->result_array();
        } else
        if ($type == 'R') {
            return $query->row();
        } else {
            return $query->result();
        }
    }

}

?>