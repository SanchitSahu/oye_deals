<?php

Class Apiv1_model extends CI_Model {

    public function Apiv1_model() {
        parent::__construct();
    }

    public function checksocialid($select = '', $where = '', $type = '') {
        $this->db->select($select);
        $this->db->from('social_login');
        $this->db->where($where);
        $query = $this->db->get();
        if ($type == 'A') {
            return $query->result_array();
        } else
        if ($type == 'R') {
            return $query->row();
        } else {
            return $query->result();
        }
    }

    public function checkuseremail($select = '', $where = '', $type = '') {
        $this->db->select($select);
        $this->db->from('users');
        if ($where) {
            $this->db->where($where);
        }
        $query = $this->db->get();
        if ($type == 'A') {
            return $query->result_array();
        } else
        if ($type == 'R') {
            return $query->row();
        } else {
            return $query->result();
        }
    }

    public function get_user($select = '', $where = '', $type = '') {
        $this->db->select($select);
        $this->db->from('users');
        if ($where) {
            $this->db->where($where);
        }
        $query = $this->db->get();
        if ($type == 'A') {
            return $query->result_array();
        } else
        if ($type == 'R') {
            return $query->row();
        } else {
            return $query->result();
        }
    }

    public function adduser($insert) {
        unset($insert['fb_id']);
        unset($insert['device_type']);
        unset($insert['device_token']);
        $result = $this->db->insert('users', $insert);
        $id = $this->db->insert_id();
        return $id;
    }

    public function addsocial($insert) {
        $result = $this->db->insert('social_login', $insert);
        $id = $this->db->insert_id();
        return $id;
    }

    public function insertauthtoken($update, $where) {
        $this->db->where($where);
        $result = $this->db->update('users', $update);
        return $result;
    }

    public function check_authtoken($auth) {
        $this->db->select('user_id');
        $this->db->from('users');
        $this->db->where(array('auth_token' => $auth));
        $query = $this->db->get();
        $user_id = $query->result();
        if (!empty($user_id)) {
            return $user_id[0]->user_id;
        } else {
            return null;
        }
    }
//Retrive token id to logout from 1 device
    public function check_devicetoken($user_id,$auth) {
        $this->db->select('token_id');
        $this->db->from('user_token');
        $this->db->where('user_id',$user_id);
        $this->db->where('device_token',$auth);
        $query = $this->db->get();
        
        $token_id = $query->result();
        if (!empty($token_id)) {
            return $token_id[0]->token_id;
        } else {
            return null;
        }
    }
    
    public function get_nearby_coupons($user_id, $latitude, $longitude, $grabbed_offers) {
        $kms = 15;
        $this->db->select("au.business_name,o.offer_image ,au.logo,o.offer_discount, o.offer_title, o.offer_desc, o.offer_enddate,ot.offer_type_name ,offer_coupons.*, (6371 * acos(cos(radians(" . $latitude . ")) * cos(radians(offer_coupons.latitude)) * cos(radians(offer_coupons.longitude) - radians(" . $longitude . ")) + sin(radians(" . $latitude . ")) * sin(radians(offer_coupons.latitude)))) AS distance");
        $this->db->from('offer_coupons');
        $this->db->join('offers o', 'o.offer_id = offer_coupons.offer_id', 'LEFT');
        $this->db->join('adminusers au', 'au.business_id = o.business_id', 'LEFT');
        $this->db->join('offer_type ot', 'ot.offer_type_id = o.offer_type_id', 'LEFT');
        //$this->db->where("offer_coupons.assigned_to = '0'");
        if ($grabbed_offers != "") {
            $this->db->where("offer_coupons.offer_id NOT IN ($grabbed_offers) AND offer_coupons.assigned_to = '0'");
        } else {
            $this->db->where("offer_coupons.assigned_to = '0'");
        }
        $this->db->having('distance <= ' . $kms);
        $this->db->order_by('distance');
        $this->db->group_by('offer_coupons.offer_id');
        $coupons = $this->db->get();
//        echo $this->db->last_query();
        return $coupons->result_array();
    }

    public function get_user_associated_offer($user_id) {
        //SELECT GROUP_CONCAT(DISTINCT offer_id) FROM offer_coupons WHERE assigned_to = 5
        $this->db->select('GROUP_CONCAT(DISTINCT offer_id) as grabbed_offers');
        $this->db->from('offer_coupons');
        $this->db->where("assigned_to = $user_id");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_user_wallet_coupons($user_id, $coupon_status) {
        $this->db->select("*, concat('" . base_url() . "assets/qr_codes/', ofc.coupon_qr_code) as coupon_qr_code, concat('" . base_url() . "assets/offerimage/', of.offer_image) as offer_image");
        $this->db->from("offer_coupons ofc");
        $this->db->join("offers of", "of.offer_id = ofc.offer_id", "LEFT");
        $this->db->join("adminusers adu", "adu.business_id = of.business_id", "LEFT");
        $this->db->where("ofc.assigned_to", $user_id);
        $this->db->where("ofc.status", $coupon_status);
        $query = $this->db->get();
        return $query->result();
    }

    public function add_fcmtoken($insert) {
        $result = $this->db->insert('user_token', $insert);
        $id = $this->db->insert_id();
        return $id;
    }

    public function capture_coupons($update, $where) {
        $this->db->where($where);
        $result = $this->db->update('offer_coupons', $update);
        return $result;
    }

    public function map_offer($data) {
        $result = $this->db->insert('offer_user_mapping', $data);
        $id = $this->db->insert_id();
        return $id;
    }

    public function logout_user($delete) {
        //in place of user_id we will logout based on token id
        $this->db->where(array('token_id' => $delete));
        $result = $this->db->delete('user_token');
        return $result;
    }
    

    public function redeem_coupon_model($coupon_id, $user_id) {
        $this->db->where(array(
            'offer_coupon_id' => $coupon_id,
            'assigned_to' => $user_id
        ));
        if ($this->db->update('offer_coupons', array('status' => '3'))) {
            return true;
        } else {
            return false;
        }
    }

    public function get_daily_offer_model($latitude, $longitude) {
        $kms = 25;
        $this->db->select("*, (6371 * acos(cos(radians(" . $latitude . ")) * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . ")) + sin(radians(" . $latitude . ")) * sin(radians(latitude)))) AS distance");
        $this->db->from('offer_coupons');
        $this->db->having('distance <= ' . $kms);
        $this->db->order_by('distance', 'asc');
        $coupons = $this->db->get();
        return $coupons->result_array();
    }

    public function get_devicetoken($select = '', $where = '', $type = '') {
        $this->db->select($select);
//        $this->db->from('device_token');
        $this->db->from('user_token');
        if ($where) {
            $this->db->where($where);
        }
        $query = $this->db->get();
        if ($type == 'A') {
            return $query->result_array();
        } else
        if ($type == 'R') {
            return $query->row();
        } else {
            return $query->result();
        }
    }

    public function update_last_activity($user_id) {
        $this->db->where(array('user_id' => $user_id));
        if ($this->db->update('users', array('last_active' => date('Y-m-d h:i:s')))) {
            return true;
        } else {
            return false;
        }
    }

    public function inactivity_cron() {
        $this->db->select('ut.user_id,ut.device_token');
        $this->db->from('users u');
        $this->db->join('user_token ut', 'ut.user_id = u.user_id', 'LEFT');
        $this->db->where('u.last_active < DATE_ADD(NOW(), INTERVAL -7 DAY)');
        $userdata = $this->db->get();
        return $userdata->result();
    }

    public function coupon_expiry_cron() {
        $this->db->select('ut.user_id, ut.device_token,o.offer_enddate');
        $this->db->from('offers o');
        $this->db->join('offer_coupons oc', 'o.offer_id = oc.offer_id', 'LEFT');
        $this->db->join('user_token ut', 'ut.user_id = oc.assigned_to', 'LEFT');
        $this->db->where('oc.status', '2');
        $userdata = $this->db->get();
        return $userdata->result();
    }

    public function getDeviceTokenArray($data = array()) {
        $deviceToken = array();
        if (!empty($data)) {
            foreach ($data as $key => $val) {
                $deviceToken[] = $val['device_token'];
            }
        }
        return $deviceToken;
    }

    // save notification to the local database 
    public function save_push_notification($userid = '', $deviceid = '', $message = '', $details = '') {
//        $detail = (!empty($details) ? $details : json_encode($details));
        $detail = $details;
        $notification_data = array('user_id' => $userid, 'title' => $message, 'details' => $detail, 'created_date' => date('Y-m-d H:i:s'));
        $result = $this->db->insert('notifications', $notification_data);
        $id = $this->db->insert_id();
        return $id;
    }

    public function get_notification_model($userid) {
        $this->db->select("*");
        $this->db->from('notifications');
        $this->db->where(array('user_id' => $userid));
        $this->db->order_by('id', 'desc');
//        $this->db->order_by('distance', 'asc');
        $notification = $this->db->get();
        return $notification->result_array();
    }
    
    public function getBusinessUsers($offer_id){
        $this->db->select("au.business_name,o.offer_id");
        $this->db->from('adminusers au');
        $this->db->join('offers o', 'o.business_id = au.business_id', 'LEFT');
        $this->db->where(array('o.offer_id'=>$offer_id));
        $query = $this->db->get();
        $business = $query->result_array();
        return $business;
    }

    public function get_coupon_status($offer_id,$offer_coupon_id){
        $this->db->select("*");
        $this->db->from('offer_coupons');
        $this->db->where(array('offer_id'=>$offer_id, 'offer_coupon_id'=>$offer_coupon_id));
        $query = $this->db->get();
        $query_result = $query->result_array();
        return $query_result;
    }
}
