<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_v1 extends CI_Controller {

    public function Api_v1() {
        parent::__construct();
        $this->load->model('webservice/Apiv1_model', 'api_model');
    }

    public function login() {
        if ($this->input->post()) {
            $details = $this->input->post();
            $checkfbid = $this->api_model->checksocialid('user_id', array('fb_id' => $details['fb_id']), 'R');
            if ($checkfbid && !empty($checkfbid)) {
                $checkemail = $this->api_model->checkuseremail('*', array('user_id' => $checkfbid->user_id), 'R');
                if ($checkemail && !empty($checkemail) && $checkemail->email == $details['email']) {
                    if ($checkemail->auth_token) {
                        $auth_token = $checkemail->auth_token;
                    } else {
                        $auth_token = getUniqueToken('users', 'auth_token');
                        $this->api_model->insertauthtoken(array('auth_token' => $auth_token), array('user_id' => $checkemail->user_id));
                    }
                    if (empty($details['device_token'])) {
                        $this->send_response('400', 'Device Token is required');
                    } else {
                        $push_data = array('user_id' => $checkemail->user_id, 'device_type' => $details['device_type'], 'device_token' => $details['device_token'], 'created_date' => date('Y-m-d h:i:s'));
                        $this->api_model->add_fcmtoken($push_data);
                        $user_data = $this->api_model->get_user('*', array('user_id' => $checkfbid->user_id), 'R');
                        if (isset($user_data->profile_pic) && !empty($user_data->profile_pic)) {
                            $user_data->profile_pic = base_url() . 'assets/profilepic/' . $user_data->profile_pic;
                        } else {
                            $user_data->profile_pic = "";
                        }
//                    send_push($details['device_token'], "Login Successfully!!!");
                        $this->send_response('200', 'Login Successfully', array($user_data));
                    }
                } else {
                    $this->send_response('400', 'Invalid Email Address');
                }
            } else {
                $checkemail = $this->api_model->checkuseremail('*', array('email' => $details['email']));
                if ($checkemail && !empty($checkemail)) {
                    $this->send_response('400', 'Email Already Registered!!');
                } else {
                    $register = $this->register($details, $_FILES);
                    if ($register) {
//                        send_push($details['device_token'], "Login Successfully!!!");
                        $user_data = $this->api_model->get_user('*', array('user_id' => $register), 'R');
                        if (isset($user_data->profile_pic) && !empty($user_data->profile_pic)) {
                            $user_data->profile_pic = base_url() . 'assets/profilepic/' . $user_data->profile_pic;
                        } else {
                            $user_data->profile_pic = "";
                        }

                        $this->send_response('200', 'Login Succesfull', array($user_data));
                    } else {
                        $this->send_response('400', 'Please try agian!!');
                    }
                }
            }
        } else {
            $this->send_response('400', 'Method not allowed!!');
        }
    }

    public function register($users = '', $file) {
        if ($file && !empty($file) && isset($file['profile_pic'])) {
            $profilepic = uploadProfilePicture($file);
            $users['profile_pic'] = $profilepic;
        }
        $users['created_date'] = date('Y-m-d h:i:s');
        $auth_token = getUniqueToken('users', 'auth_token');
        $users['auth_token'] = $auth_token;
        $add_user = $this->api_model->adduser($users);
        $social_insert = array('user_id' => $add_user, 'fb_id' => $users['fb_id'], 'created_date' => $users['created_date']);
        $this->api_model->addsocial($social_insert);
        $push_data = array('user_id' => $add_user, 'device_type' => $users['device_type'], 'device_token' => $users['device_token'], 'created_date' => $users['created_date']);
        $this->api_model->add_fcmtoken($push_data);
        if ($add_user) {
            return $add_user;
        } else {
            return 0;
        }
    }

    public function check_auth_token($auth) {
        $authcheck = $this->api_model->check_authtoken($auth);
        if ($authcheck) {
            return $authcheck;
        } else {
            return NULL;
        }
    }
    public function check_device_token($user_id,$auth) {
        $devicecheck = $this->api_model->check_devicetoken($user_id,$auth);
        if ($devicecheck) {
            return $devicecheck;
        } else {
            return NULL;
        }
    }

    public function send_response($status, $message, $data = array()) {
        echo json_encode(array('status' => $status, 'message' => $message, 'data' => $data));
    }

    public function get_nearby_coupons() {
        if ($this->input->post()) {
            $details = $this->input->post();
            $latitude = $details['latitude'];
            $longitude = $details['longitude'];
            if ($user_id = $this->check_auth_token($details['auth_token'])) {
                
                //Below function will fetch user having list of offer except he captured one
                $offers_grabbed = $this->api_model->get_user_associated_offer($user_id);
                $coupons = $this->api_model->get_nearby_coupons($user_id, $latitude, $longitude, $offers_grabbed[0]['grabbed_offers']);
                
                if (count($coupons) > 0) {
                    foreach ($coupons as &$coupon) {
                        $coupon['distance'] = (int) $coupon['distance'];
                        if (!empty($coupon['logo'])) {
                            $coupon['logo'] = base_url() . 'assets/profileimages/' . $coupon['logo'];
                        } else {
                            $coupon['logo'] = "http://via.placeholder.com/150x150";
                        }
                        if (!empty($coupon['offer_image'])) {
                            $coupon['offer_image'] = base_url() . 'assets/offerimage/' . $coupon['offer_image'];
                        } else {
                            $coupon['logo'] = "http://via.placeholder.com/150x150";
                        }
                        if (!empty($coupon['coupon_qr_code'])) {
                            $coupon['coupon_qr_code'] = base_url() . 'assets/qr_codes/' . $coupon['coupon_qr_code'];
                        } else {
                            $coupon['coupon_qr_code'] = "http://via.placeholder.com/150x150";
                        }
                    }
                    $this->send_response('200', 'Data Fetch Successfully', $coupons);
                } else {
                    $this->send_response('400', 'No Deals found at our location', array());
                }
            } else {
                $this->send_response('400', 'Authentication fail!!');
            }
        } else {
            $this->send_response('400', 'Method not allowed!!');
        }
    }

    public function get_user_wallet_coupons() {
        if ($this->input->post()) {
            $details = $this->input->post();
            $coupon_status = $details['status'];
            if ($user_id = $this->check_auth_token($details['auth_token'])) {
                $wallet = $this->api_model->get_user_wallet_coupons($user_id, $coupon_status);
                foreach ($wallet as $coupon) {
                    if (!empty($coupon->logo)) {
                        $coupon->logo = base_url() . 'assets/profileimages/' . $coupon->logo;
                    } else {
//                        $coupon['logo'] = "http://via.placeholder.com/150x150";
                        $coupon->logo = "http://via.placeholder.com/150x150";
                    }
                }
                $this->send_response('200', 'Data Fetch Successfully', $wallet);
            } else {
                $this->send_response('400', 'Authentication fail!!');
            }
        } else {
            $this->send_response('400', 'Method not allowed!!');
        }
    }

    public function capture_coupons() {
        if ($this->input->post()) {
            $details = $this->input->post();
            $offer_coupon_id = $details['offer_coupon_id'];
            $offer_id = $details['offer_id'];
            if ($user_id = $this->check_auth_token($details['auth_token'])) {
                ////check coupen is not assign to anyone else
                $coupon_status = $this->api_model->get_coupon_status($offer_id,$offer_coupon_id);
                $assigned_to = $coupon_status['0']['assigned_to'];
                
                if($assigned_to == '0'){
                    $wallet = $this->api_model->capture_coupons(array('assigned_to' => $user_id, 'status' => '2', 'updated_date' => date('Y-m-d h:i:s')), array('offer_coupon_id' => $offer_coupon_id));
                    $mapping = $this->api_model->map_offer(array('user_id' => $user_id, 'offer_id' => $offer_id, 'created_date' => date('Y-m-d h:i:s')));
                    $userdata = $this->api_model->get_devicetoken('*', array('user_id' => $user_id), 'A');
                    $deviceToken = $this->api_model->getDeviceTokenArray($userdata);

                    //Dynamic notification message
                    $coupen = $this->api_model->getBusinessUsers($offer_id);
                    $message = 'You have Captured ';
                    $message .= $coupen[0]['business_name']."'s";
                    $message .= ' Coupon Successfully!!';

                    $notification = $this->api_model->save_push_notification($user_id, $deviceToken, $message);
                    $push = send_push($deviceToken, $message);

                    if ($this->update_last_activity($user_id)) {
                        $this->send_response('200', 'Data Updated Successfully', $push);
                        exit;
                    } else {
                        $this->send_response('400', 'Activity not updated');
                        exit;
                    }
                    $this->send_response('200', 'Data Updated Successfully');
                }else {
                    $this->send_response('400', 'This Coupon is already Captured !!');
                }                
            } else {
                $this->send_response('400', 'Authentication fail!!');
            }
        } else {
            $this->send_response('400', 'Method not allowed!!');
        }
    }

    public function logout() {
        if ($this->input->post()) {
            $details = $this->input->post();
            if ($user_id = $this->check_auth_token($details['auth_token'])) {
                if($token_id = $this->check_device_token($user_id,$details['device_token'])){
                    //$getuser = $this->api_model->get_user('*', array('token_id' => $user_id), 'R');
//                    $this->api_model->logout_user($getuser->user_id);
                    $this->api_model->logout_user($token_id);
                    $this->send_response('200', 'You were successfully logged out');
                }
                else {
                    $this->send_response('400', 'Invalid auth_token or device_token!!');
                }
            } else {
                $this->send_response('400', 'Authentication fail!!');
            }
        } else {
            $this->send_response('400', 'Method not allowed!!');
        }
    }

    public function get_daily_offer() {
        if ($this->input->post()) {
            $details = $this->input->post();
            if ($user_id = $this->check_auth_token($details['auth_token'])) {
                $offer_detail = $this->api_model->get_daily_offer_model($details['latitude'], $details['longitude']);

                //SEND PUSH HERE: With data of first record from $offer_detail
                $userdata = $this->api_model->get_devicetoken('*', array('user_id' => $user_id), 'R');

                $notification = $this->api_model->save_push_notification($user_id, $userdata->device_token, 'Latest Offer', $offer_detail[0]);
                $push = send_push($userdata->device_token, 'Latest Offer', $offer_detail[0]);
                if ($push) {
                    $this->send_response('200', 'Push Sent');
                } else {
                    $this->send_response('400', 'Push Not Sent');
                }
            } else {
                $this->send_response('400', 'Authentication fail!!');
            }
        } else {
            $this->send_response('400', 'Method not allowed!!');
        }
    }

    public function redeem_coupon() {
        if ($this->input->post()) {
            $details = $this->input->post();
            if ($user_id = $this->check_auth_token($details['auth_token'])) {
                if ($this->api_model->redeem_coupon_model($details['coupon_id'], $user_id)) {
                    if ($this->update_last_activity($user_id)) {
                        $userdata = $this->api_model->get_devicetoken('*', array('user_id' => $user_id), 'R');
                        $notification = $this->api_model->save_push_notification($user_id, $userdata->device_token, 'You have Redeemed Coupon Successfully!!');
                        $push = send_push($userdata->device_token, 'You have Redeemed Coupon Successfully!!');
                        $this->send_response('200', 'Coupon Redeemed Successfully');
                    } else {
                        $this->send_response('400', 'Activity not updated');
                    }
                } else {
                    $this->send_response('400', 'Error occured while redeeming coupon');
                }
            } else {
                $this->send_response('400', 'Authentication fail!!');
            }
        } else {
            $this->send_response('400', 'Method not allowed!!');
        }
    }

    public function test_push() {
        $push = send_push('cRX3t0gZrLI:APA91bHL5WZx4rvKNhpVmQyzQHXD7hsBRPTr6wOLheyRDtpkvMPGL4eT5v7vG02A4ldfWWUFbIG5TRBxN2EqroLh4YSBCr0xqjwb5-GndnXzqVM41j9VY_EOErhhpY8ho89CqgfYqE-B', 'this is test', 'testdata');
        print_r($push);
        die;
    }

    public function update_last_activity($user_id) {

        if ($user_id != "") {
            if ($this->api_model->update_last_activity($user_id)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function inactivity_cron() {
        $user_data = $this->api_model->inactivity_cron();
        if (!empty($user_data)) {
            $msg = "You have been inactive from last 7 days!!!";
            foreach ($user_data as $token) {
                $tokens[] = $token->device_token;

                $notification = $this->api_model->save_push_notification($token->user_id, $tokens, $msg);
            }
            send_push($tokens, $msg);
        } else {
            print_r("no inactive user");
        }
    }

    public function coupon_expiry_cron() {
        $user_data = $this->api_model->coupon_expiry_cron();

        if (!empty($user_data)) {
            $msg = "You coupons will expire within 8 hours!!!";
            foreach ($user_data as $token) {
                $enddate = date('Y-m-d H:i:s', strtotime($token->offer_enddate));
                $hourdiff = round((strtotime($enddate) - strtotime(date('Y-m-d H:i:s'))) / 3600, 1);
                if ($hourdiff < 8) {
                    $user_tokens[] = $token->device_token;

                    $notification = $this->api_model->save_push_notification($token->user_id, $user_tokens, $msg);
                }
            }
            send_push($user_tokens, $msg);
        } else {
            print_r("no inactive user");
        }
    }

    public function cron_expired_coupon() {
        ///offer_coupons table > status = 4 where status != 3 and offer_enddate < curdate()
        $select_query = "SELECT * FROM offers WHERE offer_enddate < CURDATE()";
        $result = $this->db->query($select_query);
        $flag = false;
        foreach($result->result_array() as $row){
            
            //Update offer table set status to 0 of offer
            $update_offers = "UPDATE offers SET STATUS='0' WHERE offer_id = ".$row['offer_id'];
            $update_offer_result = $this->db->query($update_offers);
            
            //Update offer_coupons set status 4
            $update_query = "UPDATE offer_coupons SET STATUS='4' WHERE STATUS != '3' AND offer_id = ".$row['offer_id'];
            $update_result = $this->db->query($update_query);
            if($update_result){
                $flag = true;
            }
            else
            {
                $flag=false;
            }          
        }
        
        if($flag){
            echo "Details Updated Successfully.";
        } else {
            echo "There is some error while update coupon details";
        }
        
        
    }
    public function get_notifications() {
        if ($this->input->post()) {
            $details = $this->input->post();
            if ($user_id = $this->check_auth_token($details['auth_token'])) {
                $notification_detail = $this->api_model->get_notification_model($user_id);
                if ($notification_detail) {
                    $this->send_response('200', 'Notification fetched successfully!!!', $notification_detail);
                } else {
                    $this->send_response('400', 'Notification not fetched.');
                }
            } else {
                $this->send_response('400', 'Authentication fail!!');
            }
        } else {
            $this->send_response('400', 'Method not allowed!!');
        }
    }
    
    //Testing function to test notification in ios devices
 /*   public function test_notification(){
        if ($this->input->post()) {
            $details = $this->input->post();
            if (!empty($details['offer_id'])) {
                $coupen = $this->api_model->getBusinessUsers($details['offer_id']);
                print_r($coupen[0]['business_name']);
            } else {
                $this->send_response('400', 'Offer_id is required!!');
            }
        } else {
            $this->send_response('400', 'Method not allowed!!');
        }
    }
*/
}
