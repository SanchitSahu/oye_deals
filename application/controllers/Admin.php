<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin_login_model');
        $this->load->model('Business_type');
        $this->load->model('Admin_user');
        $this->load->model('Users_model');
        $this->load->model('Common_model');
        $data = array();
        $this->data['pageTitle'] = 'Oye Deals | Administrator';
    }

    public function index() {
        $this->common_functions->checkAdminSession();
        $this->load->view('common/header');
        $this->load->view('admin/dashboard');
        $this->load->view('common/footer');
    }

    public function login() {
        $this->data['pageTitle'] = 'Oye Deals | Administrator';
        $username = $this->input->post('admin_username');
        $password = $this->input->post('admin_password');
        $username = isset($username) ? $username : '';
        $password = isset($password) ? $password : '';
        $remember = isset($remember) ? $remember : 'off';

        if ($username != '' && $password != '') {

            $rec = $this->admin_login_model->resolve_admin_user_login($username, $password);

            if (count($rec) > 0) {

                $this->session->set_userdata('admin_user_data', $rec);
                $this->session->set_flashdata('message', 'You Have logged in successfully !!!');
                if ($remember == 'on') {
                    $this->input->set_cookie('login_admin_user', $username, time() + 86400 * 5);
                    $this->input->set_cookie('login_admin_password', $password, time() + 86400 * 5);
                }
                // Admin user login success
                /* $this->load->view('admin/header');
                  $this->load->view('admin/dashboard', $this->data);
                  $this->load->view('admin/footer'); */
                redirect('admin/dashboard');
            } else {
                // login failed
                $this->session->set_flashdata('message', 'Wrong username or password.');
                $this->load->view('admin/admin_login', $this->data);
            }
        } else {

            // login failed
            $this->session->unset_userdata('message');
            $this->session->set_flashdata('message', 'Email and Password required.');
            $this->load->view('admin/admin_login', $this->data);
        }
    }

    public function logout() {

        $this->session->userdata('admin_user_data')['email'];
        $this->session->userdata('admin_user_data')['password'];
        $this->session->sess_destroy();
        $this->session->unset_userdata('admin_user_data');
        //echo '<pre>'; print_r($this->session->userdata); exit;
        $this->input->set_cookie('login_admin_user', '', time() - 3600);
        $this->input->set_cookie('login_admin_password', '', time() - 3600);
        redirect('admin/login');
    }

    public function dashboard() {
        $this->common_functions->checkAdminSession();
        $this->data['pageTitle'] = 'Oye Deals | Dashboard';
        $this->data['businessuserlist'] = $this->Admin_user->select('*', '', '', '', 'business_id', $limit = 5);
        $this->data['appuserlist'] = $this->Users_model->select('*', '', '', '', 'user_id', $limit = 5);
        $this->data['business_types'] = $this->Business_type->select('*', '', '', '', 'business_type_id', $limit = 5);
        $this->data['total_count'] = $this->Admin_user->count_coupon();
        $this->data['all_offer'] = $this->common_model->grab_all_offer();
        $this->load->view('common/header');
        $this->load->view('admin/dashboard', $this->data);
        $this->load->view('common/footer');
    }

    public function business_type() {
        $this->data['pageTitle'] = 'Oye Deals | Nature Of Business';
        $this->common_functions->checkAdminSession();
        $data['business_types'] = $this->Business_type->grab_business_types('*', '', 'business_type_id desc');
        $this->load->view('common/header');
        $this->load->view('admin/business_type', $data);
        $this->load->view('common/footer');
    }

    public function add_business_type() {
        $post_data = $this->input->post();
        $data_array = array(
            'business_type_name' => $post_data['business_type_name'],
            'status' => $post_data['status_bussines'],
            'created_date' => date('Y-m-d h:i:s'),
            'updated_date' => date('Y-m-d h:i:s')
        );
        if ($post_data['mode'] == 'save') {
            $result = $this->Business_type->add_business_type($data_array);
        } else {
            $where = 'business_type_id = ' . $post_data['upid'];
            $result = $this->Business_type->update_business_type($data_array, $where);
        }
        die(json_encode($result));
    }

    public function get_business_type() {
        $id = $this->input->post('id');
        $result = $this->Business_type->grab_business_types('*', $id);
        die(json_encode($result));
    }

    public function app_users() {
        $this->data['pageTitle'] = 'Oye Deals | App Users';
        $this->common_functions->checkAdminSession();
        $this->data['appuserlist'] = $this->Admin_user->getAppUsers();
        $this->load->view('common/header');
        $this->load->view('admin/app_users', $this->data);
        $this->load->view('common/footer');
    }

    public function change_app_user_status() {
        $post_data = $this->input->post();
//        echo "<pre>";print_r($post_data);exit;
        $result = '';
        if (isset($post_data['status']) && !empty($post_data['status'])) {
//            echo "<pre>";print_r($post_data['status']);exit;
            foreach ($post_data['status'] as $key => $val) {
                // $key is primary id of the field
                //$val is new  status value of users
                $this->db->where('user_id', $key);
                $result = $this->db->update('users', array('status' => $val));
            }
        }
        echo json_encode(array("message" => "Success"));
        exit;
    }

    public function business_users() {
        $this->data['pageTitle'] = 'Oye Deals | Business Users';
        $this->common_functions->checkAdminSession();
        $this->data['businessuserlist'] = $this->Admin_user->getBusinessUsers();
        $this->load->view('common/header');
        $this->load->view('admin/business_users', $this->data);
        $this->load->view('common/footer');
    }

    public function change_business_user_status() {
        $post_data = $this->input->post();
        $result = '';
        if (isset($post_data['status']) && !empty($post_data['status'])) {
            foreach ($post_data['status'] as $key => $val) {
                $this->db->where('business_id', $key);
                $result = $this->db->update('adminusers', array('status' => $val));
            }
        }
        echo json_encode(array("message" => "Success"));
        exit;
    }
    public function analytics()
    {
        $this->data['pageTitle'] = 'Oye Deals | Analytics';
        $this->common_functions->checkAdminSession();
        $this->load->view('common/header');
        $this->load->view('admin/analytics', $this->data);
        $this->load->view('common/footer');
    }
    public function grab_analytics()
    {
        $post_data = $this->input->post();
        $result = $this->Common_model->grab_analytics_coupon($post_data['coupon_type'],date('Y-m-d 00:00:00',strtotime($post_data['start_date'])),date('Y-m-d 23:59:59',strtotime($post_data['end_date'])));
        //echo "<pre>"; print_r($result); exit;
        if($result> 0){
        $table_data ="<table class='table table-striped table-bordered'>
                      <thead>
                        <tr>
                          <th>Active </th>
                          <th>Grabbed</th>
                          <th>Redeemed</th>
                          <th>Expired</th>
                        </tr>
                      </thead>
                      <tbody>";
        foreach ($result as $key => $value) { 
                        $table_data .="<tr>";
                       // $table_data .="<td>".$value['created_date']."</td>";
                        //$table_data .="<td>".$value['coupon_code']."</td>";
                        $table_data .="<td>";
                        $table_data .=isset($value['coupon_1'])?$value['coupon_1']:'-'."</td>";
                        $table_data .="<td>";
                        $table_data .=isset($value['coupon_2'])?$value['coupon_2']:'-'."</td>";
                        $table_data .="<td>";
                        $table_data .=isset($value['coupon_3'])?$value['coupon_3']:'-'."</td>";
                        $table_data .="<td>";
                        $table_data .=isset($value['coupon_4'])?$value['coupon_4']:'-'."</td>";
                        $table_data .="</tr>";
                    }
                        
        echo $table_data .="</tbody></table>";
        }
        else{
            echo "There is no record";
        }
        exit;
    }
    public function offers() {
        $this->data['pageTitle'] = 'Offers | Oye Deals';
        $this->common_functions->checkAdminSession();
        $this->data['offer_type'] = $this->common_model->grab_offer_types();
        $all_offer = $this->common_model->grab_all_offer();
        foreach ($all_offer as &$data) {
            $data['coupon_count'] = $this->common_model->grab_avail_cap_coupon_count($data['offer_id']);
            if (date("Y-m-d") > $data['offer_startdate'] && date("Y-m-d") < $data['offer_enddate']) {
                $data['status_text'] = "In Progress";
            } else if (date("Y-m-d") < $data['offer_startdate']) {
                $data['status_text'] = "Yet to start";
            } else if (date("Y-m-d") > $data['offer_enddate']) {
                $data['status_text'] = "Expired";
            }
        }
        $this->data['all_offer'] = $all_offer;
        $this->load->view('common/header', $this->data);
        $this->load->view('admin/offers');
        $this->load->view('common/footer');
    }
    
}
