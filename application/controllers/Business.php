<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Admin_user');
        $this->load->model('business_type');
        $this->load->model('Users_model');
        $this->load->model('common_model');
        // Your own constructor code
    }

    public function index() {
        $this->common_functions->checkBusinessSession();
        $this->load->view('common/business_header');
        $this->load->view('business/dashboard');
        $this->load->view('common/business_footer');
    }

    public function businesslogin() {
//        $this->common_functions->checkBusinessSession();
//        $this->load->view('common/business_header');
        $this->load->view('index');
//        $this->load->view('common/business_footer');
    }

    public function Createprofile() {
        //$this->load->view('common/business_header');
        $data['business_type'] = $this->business_type->grab_business_types();
        $this->load->view('business/createprofile', $data);
        //$this->load->view('common/business_footer');
    }

    public function register() {
        $bname = $_POST['bname'];
        $email = $_POST['email'];
        $password = md5($_POST['password']);
        $data = array(
            'business_name' => $bname,
            'email_id' => $email,
            'password' => $password,
        );
        $result = $this->Admin_user->insert_data($data);
        $data1['business_type'] = $this->business_type->grab_business_types();
        if ($result > 0) {
            $rec = array(
                'BusinessUserId' => $result,
                'BusinessUserName' => $bname,
            );
            $this->session->set_userdata('business_user_data', $rec);
            $this->load->view('business/createprofile', $data1);
        }
    }

    public function login() {

        $email = $_POST['email'];
        $pass = $_POST['pwd'];
        $password = md5($pass);
        $data = array(
            'email_id' => $email,
            'password' => $password,
        );

        $result = $this->Admin_user->select('business_id,business_name,logo', $data, 'R');
        //echo '<pre>'; print_r($result); exit;
        $bid = $result->business_id;
        $bname = $result->business_name;
        $logo = $result->logo;

        if ($result != '') {
            $rec = array(
                'BusinessUserId' => $bid,
                'BusinessUserName' => $bname,
                'BusinessUserLogo' => $logo
            );

            $this->session->set_userdata('business_user_data', $rec);
            $this->session->set_flashdata('sussess', 'You Have logged in successfully !!!');
            redirect('business/index');
        } else {
            $this->session->set_flashdata('error', 'You Have logged in successfully !!!');
            redirect(base_url());


            $result = $this->Admin_user->select('business_id,business_name', $data, 'R');
            //  echo '<pre>'; print_r($result); exit;
            $bid = $result->business_id;
            $bname = $result->business_name;

            if ($result != '') {
                $rec = array(
                    'BusinessUserId' => $bid,
                    'BusinessUserName' => $bname,
                );
                $this->session->set_userdata('business_user_data', $rec);
                //$this->session->set_userdata('BusinessUserId', $bid);
                //$this->session->set_userdata('BusinessUserName', $bname);
                $this->session->set_flashdata('success', 'You Have logged in successfully !!!');
                redirect('business/index');
            } else {
                $this->session->set_flashdata('error', 'You Have logged in successfully !!!');
                redirect(base_url());
            }
        }
    }

    public function mangecoupons() {
        $this->common_functions->checkBusinessSession();
        $this->load->view('common/business_header');
        $this->load->view('business/mange_coupons');
        $this->load->view('common/business_footer');
    }

    public function viewprofile() {
        $this->common_functions->checkBusinessSession();
        $data['profile'] = $this->Admin_user->getProfile();
        $data['business_type'] = $this->business_type->grab_business_types();
        //echo '<pre>'; print_r($data); exit;
        $this->load->view('common/business_header');
        $this->load->view('business/profile', $data);
        $this->load->view('common/business_footer');
    }

    public function updateprofile() {
        $this->common_functions->checkBusinessSession();
        $add_profile = $this->Admin_user->updateProfile($_POST, $_FILES);
        redirect('business/viewprofile');
    }

    public function offers() {
        $this->data['pageTitle'] = 'Offers | Oye Deals';
        $this->common_functions->checkBusinessSession();
        $this->data['offer_type'] = $this->common_model->grab_offer_types();
        $all_offer = $this->common_model->grab_all_offer();
        foreach ($all_offer as &$data) {
            $data['coupon_count'] = $this->common_model->grab_avail_cap_coupon_count($data['offer_id']);
            if (date("Y-m-d") > $data['offer_startdate'] && date("Y-m-d") < $data['offer_enddate']) {
                $data['status_text'] = "In Progress";
            } else if (date("Y-m-d") < $data['offer_startdate']) {
                $data['status_text'] = "Yet to start";
            } else if (date("Y-m-d") > $data['offer_enddate']) {
                $data['status_text'] = "Expired";
            }
        }
        $this->data['all_offer'] = $all_offer;
        $this->load->view('common/business_header', $this->data);
        $this->load->view('offers/offers');
        $this->load->view('common/business_footer');
    }

    public function manage_offers($offer_id) {
        $this->data['pageTitle'] = 'Offers Details | Oye Deals';
        $this->common_functions->checkBusinessSession();
        $this->data['offer_type'] = $this->common_model->grab_offer_types();
        $this->data['offer_data'] = $this->common_model->get_offer_data($offer_id);
        $this->data['offer_coupon_data'] = $this->common_model->get_offer_coupon_data($offer_id);
        foreach ($this->data['offer_coupon_data'] as &$coupon) {
            if ($coupon['status'] == '1') {
                $coupon['status_text'] = "Available";
            } else if ($coupon['status'] == '2') {
                $coupon['status_text'] = "Captured";
            } else if ($coupon['status'] == '3') {
                $coupon['status_text'] = "Redeemed";
            } else if ($coupon['status'] == '4') {
                $coupon['status_text'] = "Expired";
            }
        }
//        echo "<pre>";
//        print_r($this->data);
//        exit;

        $this->load->view('common/business_header', $this->data);
        $this->load->view('offers/manage_offers');
        $this->load->view('common/business_footer');
    }

    public function save_offer_details() {
        $this->load->library('CSVReader');
        $this->common_functions->checkBusinessSession();
        $couponData = $this->csvreader->parse_file($_FILES['couponData']['tmp_name']); //path to csv file

        $location = explode(",", $this->input->post('location'));

        $insert_data = array(
            'business_id' => $this->session->userdata('business_user_data')['BusinessUserId'],
            'offer_type_id' => $this->input->post('offer_type_id'),
            'offer_discount' => $this->input->post('offer_discount'),
            'offer_title' => $this->input->post('offer_title'),
            'offer_desc' => $this->input->post('offer_desc'),
            'offer_startdate' => date('Y-m-d', strtotime($this->input->post('offer_startdate'))),
            'offer_enddate' => date('Y-m-d', strtotime($this->input->post('offer_enddate'))),
            'latitude' => $location[0],
            'longitude' => $location[1],
            'offer_area' => $this->input->post('offer_area'),
            'radius' => $this->input->post('radius'),
            'created_date' => date('Y-m-d H:i:s'),
            'updated_date' => date('Y-m-d H:i:s'),
        );

        $config['upload_path'] = './assets/offerimage/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 100000;
        $config['max_width'] = 2048;
        $config['max_height'] = 2048;
        $this->load->library("upload");
        $this->upload->initialize($config);
        $this->upload->do_upload('offer_image');
        $data = $this->upload->data();
        $insert_data['offer_image'] = $data['file_name'];

        $offer_id = $this->Admin_user->save_offer_details_model($insert_data);
        if ($offer_id) {
            foreach ($couponData as $key => $coupon) {
                $longitude = (float) $location[1];
                $latitude = (float) $location[0];
                $radius = rand(1, 10);

                $lng_min = $longitude - $radius / abs(cos(deg2rad($latitude)) * 69);
                $lng_max = $longitude + $radius / abs(cos(deg2rad($latitude)) * 69);
                $lat_min = $latitude - ($radius / 69);
                $lat_max = $latitude + ($radius / 69);

                if ($key % 2 == 0) {
                    $this->Admin_user->save_offer_coupon_model($offer_id, $coupon, $lat_min, $lng_min);
                } else {
                    $this->Admin_user->save_offer_coupon_model($offer_id, $coupon, $lat_max, $lng_max);
                }
            }
            $this->session->set_flashdata('success', 'Offer added Successfully');
            redirect('business/offers');
        } else {
            $this->session->set_flashdata('error', 'Error occured while adding Offer');
            redirect('business/offers');
        }
    }

    public function customers() {
        $data['users'] = $this->Users_model->selectcustomer();
        $this->common_functions->checkBusinessSession();
        $this->load->view('common/business_header');
        $this->load->view('business/customers', $data);
        $this->load->view('common/business_footer');
    }

    public function logout() {

        $this->session->sess_destroy();
        //$this->session->unset_userdata();
        //echo '<pre>'; print_r($this->session->userdata); exit;
        redirect(base_url().'business/businesslogin');
    }

}
