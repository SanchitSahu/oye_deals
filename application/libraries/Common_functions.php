<?php

class Common_functions {

    public $ci;

    public function __construct() {
        $this->ci = & get_instance();
        $this->no_cache();
    }

    public function checkAdminLogin() {

        if ($this->ci->uri->segment(2) != 'logout') {
            if ($this->ci->input->cookie('login_admin_user') != '' || $this->ci->session->userdata('admin_user_data') != '') {
                if ($this->ci->input->cookie('login_admin_user') != '') {
                    $username = $this->ci->input->cookie('login_admin_user');
                    $password = $this->ci->input->cookie('login_admin_password');
                }

                if ($this->ci->session->userdata('admin_user_data') != '') {
                    $username = $this->ci->session->userdata('admin_user_data')['email'];
                    $password = $this->ci->session->userdata('admin_user_data')['password'];
                }

                $res = $this->getRecordById('superadmin', 'id,email,name,password,profilepic,updated_date,created_date', array('email' => $username, 'password' => $password));
                $this->ci->session->set_userdata('admin_user_data', (array) $res);
                if ($this->ci->uri->segment(1) == 'login' && count($res) > 0) {
                    redirect('admin/dashboard');
                }
                if (count($res) < 1) {
                    redirect('admin/logout');
                }
            }
        }
    }

    public function checkAdminSession() {
        if (empty($this->ci->session->userdata('admin_user_data'))) {
            redirect('admin/login');
        }
    }

    public function checkBusinessSession() {
        if (empty($this->ci->session->userdata('business_user_data'))) {
            redirect('business/businesslogin');
        }
    }

    public function no_cache() {
        $this->ci->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->ci->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->ci->output->set_header('Pragma: no-cache');
        $this->ci->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    }

    public function getRecordById($table, $select, $cond, $count = 0) {

        $this->ci->db->where($cond);
        $this->ci->db->select($select);
        $q = $this->ci->db->get($table);

        if ($count > 0) {
            return $q->num_rows();
        } else {
            return $q->row();
        }
    }

    public function displayStatus($status = 0) {
        if ($status == 1) {
            $return_status = "Active";
        } else {
            $return_status = "Inactive";
        }
        return $return_status;
    }

    public function get_areaname($areaid) {
        $this->ci->db->where('area_id', $areaid);
        $this->ci->db->select('area_name');
        $res = $this->ci->db->get('area');
        $result = $res->result();
        return $result[0]->area_name;
    }

    public function get_cityname($cityid) {
        $this->ci->db->where('city_id', $cityid);
        $this->ci->db->select('city_name');
        $res = $this->ci->db->get('cities');
        $result = $res->result();
        return $result[0]->city_name;
    }

    public function get_statename($stateid) {
        $this->ci->db->where('state_id', $stateid);
        $this->ci->db->select('state_name');
        $res = $this->ci->db->get('states');
        $result = $res->result();
        return $result[0]->state_name;
    }

    public function get_countryname($countryid) {
        $this->ci->db->where('country_id', $countryid);
        $this->ci->db->select('country_name');
        $res = $this->ci->db->get('countries');
        $result = $res->result();
        return $result[0]->country_name;
    }

    public function grabbedcount($userid = '') {

        $Uid = $this->ci->session->userdata('business_user_data')['BusinessUserId'];

        if($userid != '')
        {
                $cond = array('oc.assigned_to' => $userid, 'oc.status' => '2', 'o.business_id' => $Uid);
        }
        else {
                $cond = array('oc.status' => '2', 'o.business_id' => $Uid);

        }
        $this->ci->db->where($cond);
        $this->ci->db->select('count(*) as cnt');
        $this->ci->db->from('offer_coupons AS oc');
        $this->ci->db->join('offers AS o', 'o.offer_id = oc.offer_id', 'INNER');
        $res = $this->ci->db->get();
        //echo $this->ci->db->last_query(); exit;
        $result = $res->result();
        return $result[0]->cnt;
    }

    public function redeemedcount($userid) {
//        if ($this->session->userdata('business_user_data') != null && $this->session->userdata('business_user_data') != "") {
        $Uid = $this->ci->session->userdata('business_user_data')['BusinessUserId'];
//        } else {
//            $Uid = $this->session->userdata()['BusinessUserId'];
//        }
        $cond = array('oc.assigned_to' => $userid, 'oc.status' => '3', 'o.business_id' => $Uid);
        $this->ci->db->where($cond);
        $this->ci->db->select('count(*) as cnt');
        $this->ci->db->from('offer_coupons AS oc');
        $this->ci->db->join('offers AS o', 'o.offer_id = oc.offer_id', 'INNER');
        $res = $this->ci->db->get();
        //echo $this->ci->db->last_query(); exit;
        $result = $res->result();
        return $result[0]->cnt;
    }


    public function get_activecouponcount() {
         $Uid = $this->ci->session->userdata('business_user_data')['BusinessUserId'];
        $this->ci->db->select('COUNT(*) as cnt');
        $this->ci->db->from('offer_coupons oc');
        $this->ci->db->join('offers o', 'o.offer_id = oc.offer_id', 'INNER');
        $this->ci->db->join('adminusers au', 'au.business_id = o.business_id', 'INNER');
        $cond = array(
            'o.business_id' => $Uid,
            'oc.status' => '1'
        );
        $this->ci->db->where($cond);
        $res = $this->ci->db->get();
        $result = $res->result();
        return $result[0]->cnt;
    }

    public function get_grabedcouponcount() {
        $Uid = $this->ci->session->userdata('business_user_data')['BusinessUserId'];
        $this->ci->db->select('COUNT(*) as cnt');
        $this->ci->db->from('offer_coupons oc');
        $this->ci->db->join('offers o', 'o.offer_id = oc.offer_id', 'INNER');
        $this->ci->db->join('adminusers au', 'au.business_id = o.business_id', 'INNER');
        $cond = array(
            'o.business_id' => $Uid,
            'oc.status' => '2'
        );
        $this->ci->db->where($cond);
        $res = $this->ci->db->get();
        $result = $res->result();
        return $result[0]->cnt;
    }

    public function get_redeemcouponcount() {
        $Uid = $this->ci->session->userdata('business_user_data')['BusinessUserId'];
        $this->ci->db->select('COUNT(*) as cnt');
        $this->ci->db->from('offer_coupons oc');
        $this->ci->db->join('offers o', 'o.offer_id = oc.offer_id', 'INNER');
        $this->ci->db->join('adminusers au', 'au.business_id = o.business_id', 'INNER');
        $cond = array(
            'o.business_id' => $Uid,
            'oc.status' => '3'
        );
        $this->ci->db->where($cond);
        $res = $this->ci->db->get();
        $result = $res->result();
        return $result[0]->cnt;
    }

    public function get_expiredcouponcount() {
        $Uid = $this->ci->session->userdata('business_user_data')['BusinessUserId'];
        $this->ci->db->select('COUNT(*) as cnt');
        $this->ci->db->from('offer_coupons oc');
        $this->ci->db->join('offers o', 'o.offer_id = oc.offer_id', 'INNER');
        $this->ci->db->join('adminusers au', 'au.business_id = o.business_id', 'INNER');
        $cond = array(
            'o.business_id' => $Uid,
            'oc.status' => '4'
        );
        $this->ci->db->where($cond);
        $res = $this->ci->db->get();
        $result = $res->result();
        return $result[0]->cnt;
    }
}

?>